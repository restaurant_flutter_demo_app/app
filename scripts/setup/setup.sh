#!/bin/sh

ls &&
fvm flutter pub add freezed
fvm flutter pub add mason
fvm flutter pub add golden_toolkit
fvm flutter pub add flutter_svg
fvm flutter pub add build_runner
fvm flutter pub add cached_network_image
fvm flutter pub add json_annotation
fvm flutter pub add get_it
fvm flutter pub add flutter_localization
fvm flutter pub add flutter_bloc
fvm flutter pub add swagger_dart_code_generator
fvm flutter pub add chopper
fvm flutter pub add chopper_generator
fvm flutter pub add yaml
fvm flutter pub add json_serializable
fvm flutter pub add flutter_secure_storage
fvm flutter pub add flutter_localizations --sdk=flutter
fvm flutter pub add intl:any
fvm flutter pub add drift
fvm flutter pub add drift_dev
fvm flutter pub add path_provider
fvm flutter pub add path
fvm flutter pub add sqlite3_flutter_libs
fvm flutter pub add sqlite3
fvm flutter pub add build
fvm flutter pub add freezed_annotation
fvm flutter pub add uuid

fvm flutter pub get

mason init
mason upgrade
mason add ui_component --path gen/bricks/ui_component
mason add page --path gen/bricks/page
