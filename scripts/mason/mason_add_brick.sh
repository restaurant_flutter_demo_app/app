#!/bin/sh

BRICK_PATH="./gen/bricks/"
BRICK_NAME="ui_component_test"

cd ../../ &&
ls &&
mason init
mason new $BRICK_NAME -o $BRICK_PATH
