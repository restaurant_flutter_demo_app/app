BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "OrderPlace" (
	"Uuid"	TEXT NOT NULL UNIQUE,
	"Datetime"	INTEGER NOT NULL,
	"PlaceUuid"	TEXT NOT NULL,
	"IsComplete"	INTEGER NOT NULL,
	PRIMARY KEY("Uuid")
);
CREATE TABLE IF NOT EXISTS "OrderProduct" (
	"Uuid"	TEXT NOT NULL UNIQUE,
	"Datetime"	INTEGER NOT NULL,
	"ProductUuid"	TEXT NOT NULL,
	"OrderUuid"	TEXT NOT NULL,
	"Count"	INTEGER NOT NULL CHECK(Count>0),
	PRIMARY KEY("Uuid"),
	FOREIGN KEY("OrderUuid") REFERENCES "OrderPlace"("Uuid"),
	FOREIGN KEY("ProductUuid") REFERENCES "Product"("Uuid")
);
CREATE TABLE IF NOT EXISTS "Place" (
	"Uuid"	TEXT NOT NULL UNIQUE,
	"Name"	TEXT NOT NULL UNIQUE,
	PRIMARY KEY("Uuid")
);
CREATE TABLE IF NOT EXISTS "Product" (
	"Uuid"	TEXT NOT NULL UNIQUE,
	"Name"	TEXT NOT NULL UNIQUE,
	"Price"	INTEGER NOT NULL,
	"PriceCoins"	INTEGER NOT NULL,
	"ProductTypeUuid"	TEXT NOT NULL,
	PRIMARY KEY("Uuid")
);
CREATE TABLE IF NOT EXISTS "ProductType" (
	"Uuid"	TEXT NOT NULL UNIQUE,
	"Name"	TEXT,
	PRIMARY KEY("Uuid")
);
INSERT INTO "OrderPlace" ("Uuid","Datetime","PlaceUuid","IsComplete") VALUES ('e88ff40d-2606-45c1-af7b-c41c763fd2eb',1716445527,'0a2c52d3-2db7-47f8-a846-a3bb6c2f1711',0);
INSERT INTO "OrderProduct" ("Uuid","Datetime","ProductUuid","OrderUuid","Count") VALUES ('26e91701-c171-487a-a099-d8989dc1dd21',1716446037,'d4e4dbc1-9845-4fda-a753-897cf6a83fda','e88ff40d-2606-45c1-af7b-c41c763fd2eb',2);
INSERT INTO "Place" ("Uuid","Name") VALUES ('0a2c52d3-2db7-47f8-a846-a3bb6c2f1711','Стол 1'),
 ('0a2c52d3-2db7-47f8-a846-a3bb6c2f1712','Стол 2');
INSERT INTO "Product" ("Uuid","Name","Price","PriceCoins","ProductTypeUuid") VALUES ('d4e4dbc1-9845-4fda-a753-897cf6a83fda','Coca Cola 0.5 л',30,0,'9fd6a1b1-1853-4126-8806-7326ed063eb7'),
 ('09d528e0-97c1-4399-9973-8bdb5cb8d7b4','Мороженное',150,0,'2ce30850-f3c2-43d2-a605-152cb39adb0a'),
 ('d4e4dbc1-9845-4fda-a753-897cf6a83fdb','Fanta 0.5 л',30,0,'9fd6a1b1-1853-4126-8806-7326ed063eb7'),
 ('09d528e0-97c1-4399-9973-8bdb5cb8d7b5','Мороженное Ванильное',160,0,'2ce30850-f3c2-43d2-a605-152cb39adb0a');
INSERT INTO "ProductType" ("Uuid","Name") VALUES ('9fd6a1b1-1853-4126-8806-7326ed063eb7','Напитки'),
 ('e26f7a9e-0e74-476e-9e36-078a0ff8b18f','Закуски'),
 ('552941be-74cc-4922-b8a6-42d261c7f681','Первые блюда'),
 ('045c5675-26d4-47e5-b6c3-c0bbd448f277','Гарниры'),
 ('2ce30850-f3c2-43d2-a605-152cb39adb0a','Десерты');
CREATE VIEW OrderPlaceView
AS
SELECT
OrderPlace.Uuid,
OrderPlace.Datetime,
OrderPlace.IsComplete,
OrderPlace.PlaceUuid,
Place.Name
FROM OrderPlace
INNER join Place on OrderPlace.PlaceUuid= Place.Uuid;
CREATE VIEW OrderProductView
AS
SELECT
OrderProduct.Uuid,
OrderProduct.Count,
OrderProduct.Datetime,
OrderProduct.OrderUuid,
ProductView.Uuid as ProductUuid,
ProductView.Name,
ProductView.Price,
ProductView.PriceCoins,
ProductView.ProductTypeUuid,
ProductView.ProductTypeName
FROM OrderProduct
INNER join ProductView on OrderProduct.ProductUuid = ProductView.Uuid;
CREATE VIEW ProductView
AS
SELECT
Product.Uuid,
Product.Name,
Product.Price,
Product.PriceCoins,
Product.ProductTypeUuid,
ProductType.Name as ProductTypeName
FROM Product
INNER join ProductType on Product.ProductTypeUuid = ProductType.Uuid;
COMMIT;
