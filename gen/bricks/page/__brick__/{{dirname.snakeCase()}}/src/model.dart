import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class {{classname.pascalCase()}}PageModel with _${{classname.pascalCase()}}PageModel {
  const factory {{classname.pascalCase()}}PageModel({
    String? title,
  }) = _{{classname.pascalCase()}}PageModel;
}
