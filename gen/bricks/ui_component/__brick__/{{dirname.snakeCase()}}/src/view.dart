
part of '../index.dart';

class {{classname.pascalCase()}} extends StatelessWidget {
{{classname.pascalCase()}}({
super.key,
required {{classname.pascalCase()}}Model model,
}) : controller = {{classname.pascalCase()}}Controller(
model: model
);

final {{classname.pascalCase()}}Controller controller;

@override
Widget build(BuildContext context) {
final theme = Theme.of(context);
final textTheme = theme.textTheme;

final textPrimaryColors = theme.extension<TextPrimaryColors>()!;

return ValueListenableBuilder<{{classname.pascalCase()}}Model>(
valueListenable: controller,
builder: (BuildContext context, {{classname.pascalCase()}}Model value, Widget? child,) {
return const SizedBox.shrink();
},);

}
}
