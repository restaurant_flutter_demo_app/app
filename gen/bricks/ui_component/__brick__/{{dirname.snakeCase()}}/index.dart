import 'package:ui_kit/src/resources/index.dart';
import 'package:flutter/material.dart';


export 'src/model.dart';

part 'src/controller.dart';
part 'src/view.dart';
