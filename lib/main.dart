import 'dart:async';

import 'package:app/src/app/extra/util/navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:ui_kit/ui_kit.dart';

import 'src/data/extra/services/services.dart';

Future<void> main() async {
  Bloc.observer = const GlobalObserver();
  Services.initNavigationService();

  final app = MaterialApp(
    theme: Themes.defaultTheme,
    navigatorKey: GetIt.I.get<INavigationService>().navigatorKey,
    title: 'Restaurant Orders',
    routes: GetIt.I.get<INavigationService>().routes,
    onGenerateRoute: GetIt.I.get<INavigationService>().onGenerateRoute,
    localizationsDelegates: AppLocalizations.localizationsDelegates,
    supportedLocales: AppLocalizations.supportedLocales,
    initialRoute: GetIt.I.get<INavigationService>().initRouteName,
  );

  //Flogger.init();

  runApp(
    _App(
      child: app,
    ),
  );
}

/// {@template counter_observer}
/// [BlocObserver] for the counter application which
/// observes all state changes.
/// {@endtemplate}
class GlobalObserver extends BlocObserver {
  /// {@macro counter_observer}
  const GlobalObserver();

  @override
  void onChange(BlocBase<dynamic> bloc, Change<dynamic> change) {
    super.onChange(bloc, change);
    // ignore: avoid_print
    print('${bloc.runtimeType} $change');
  }
}

class _App extends StatefulWidget {
  final MaterialApp child;

  const _App({required this.child});

  @override
  State<_App> createState() {
    return _AppState();
  }
}

class _AppState extends State<_App> with WidgetsBindingObserver {
  @override
  void initState() {
    Services.initServices();

    super.initState();
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.resumed:
        break;
      default:
        break;
    }

    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
