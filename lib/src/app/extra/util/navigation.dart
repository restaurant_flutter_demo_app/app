import 'package:app/src/app/pages/order_create/index.dart';
import 'package:app/src/app/pages/order_details/index.dart';
import 'package:app/src/app/pages/order_list/index.dart';
import 'package:app/src/app/pages/product_list/index.dart';
import 'package:flutter/material.dart';

class PathRoutes {
  static const orderList = '/order_list';
  static const orderDetails = '/order_detail';
  static const orderCreate = '/order_create';
  static const placeList = '/place_list';
  static const productListDetails = '/product_list';
}

abstract class INavigationService {
  final Map<String, Widget Function(BuildContext)> routes = {};

  RouteFactory? get onGenerateRoute;

  String get initRouteName;

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  /// Routes
  bool canPop();
  void routePop();
  void routeOrderList();
  Future<void> routePlaceList();
  Future<void> routeOrderDetails(
    OrderDetailsPageArgs args,
  );
  void routeOrderCreate(
    OrderDetailsPageArgs args,
  );
  Future<void> routeProductList(
    ProductListPageArgs args,
  );
}

class NavigationService implements INavigationService {
  @override
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  String get initRouteName => PathRoutes.orderList;

  @override
  final routes = <String, Widget Function(BuildContext)>{
    PathRoutes.placeList: (context) => const OrderCreatePageView(),
  };

  @override
  RouteFactory? get onGenerateRoute => (settings) {
        switch (settings.name) {
          case PathRoutes.orderList:
            return MaterialPageRoute(
              builder: (context) => OrderListPageView(
                args: settings.arguments == null ? OrderListPageArgs() : settings.arguments as OrderListPageArgs,
              ),
            );
          case PathRoutes.orderCreate:
          case PathRoutes.orderDetails:
            return MaterialPageRoute(
              builder: (context) => OrderDetailsPageView(
                args: settings.arguments as OrderDetailsPageArgs,
              ),
            );
          case PathRoutes.productListDetails:
            return MaterialPageRoute(
              builder: (context) => ProductListPageView(
                args: settings.arguments as ProductListPageArgs,
              ),
            );
        }
        return null;
      };

  @override
  void routePop() => Navigator.pop(navigatorKey.currentContext!);

  @override
  void routeOrderList() {
    Navigator.pushReplacementNamed(
      navigatorKey.currentContext!,
      PathRoutes.orderList,
    );
  }

  @override
  bool canPop() {
    return Navigator.canPop(navigatorKey.currentContext!);
  }

  @override
  Future<void> routeOrderDetails(
    OrderDetailsPageArgs args,
  ) async =>
      await Navigator.pushNamed(
        navigatorKey.currentContext!,
        PathRoutes.orderDetails,
        arguments: args,
      );

  @override
  Future<void> routeProductList(ProductListPageArgs args) async {
    await Navigator.pushNamed(
      navigatorKey.currentContext!,
      PathRoutes.productListDetails,
      arguments: args,
    );
  }

  @override
  Future<void> routePlaceList() async {
    await Navigator.pushNamed(
      navigatorKey.currentContext!,
      PathRoutes.placeList,
    );
  }

  @override
  void routeOrderCreate(OrderDetailsPageArgs args) {
    Navigator.pushReplacementNamed(
      navigatorKey.currentContext!,
      arguments: args,
      PathRoutes.orderCreate,
    );
  }
}
