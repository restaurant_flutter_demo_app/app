import 'package:intl/intl.dart';

class DatetimeFormat {
  static String getFormattedApi({
    required int? timestamp,
  }) {
    final dateTime =
        DateTime.fromMillisecondsSinceEpoch((timestamp ?? 0) * 1000);
    final formatter = DateFormat('dd/MM/yyyy (HH:mm)');
    return formatter.format(dateTime);
  }

  static String getFormattedLocale({
    required DateTime? dateTime,
  }) {
    final formatter = DateFormat('dd/MM/yyyy (HH:mm)');
    return formatter.format(
      dateTime ?? DateTime.now(),
    );
  }

  static DateTime getDateimeByTimestampApi({
    required int? timestamp,
  }) {
    return DateTime.fromMillisecondsSinceEpoch((timestamp ?? 0) * 1000);
  }
}
