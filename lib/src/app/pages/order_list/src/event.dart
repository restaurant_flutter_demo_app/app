part of '../index.dart';

abstract class AOrderListPageEvent {}

final class OrderListPageInitLoadEvent extends AOrderListPageEvent {
  OrderListPageInitLoadEvent();
}

final class OrderListPageSearchEvent extends AOrderListPageEvent {
  OrderListPageSearchEvent({
    required this.query,
  });

  final String query;
}

final class OrderListPageRemoveEvent extends AOrderListPageEvent {
  OrderListPageRemoveEvent({
    required this.index,
  });

  final int index;
}
