part of '../index.dart';

class OrderListPageArgs {
  OrderListPageArgs();
}

class OrderListPageView extends StatefulWidget {
  const OrderListPageView({
    required this.args,
    super.key,
  }) : super();

  final OrderListPageArgs args;

  @override
  State<OrderListPageView> createState() => _OrderListPageViewState();
}

class _OrderListPageViewState extends State<OrderListPageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => OrderPageBlocController(args: widget.args),
      child: BlocBuilder<OrderPageBlocController, OrderListPageStateModel>(
        builder: (
          context,
          state,
        ) {
          final OrderPageBlocController controller = context.read<OrderPageBlocController>();

          return Scaffold(
            appBar: OrderListAppBar(
              title: AppLocalizations.of(context)!.orders,
              onAddTap: () async {
                await GetIt.I.get<INavigationService>().routePlaceList();
                controller.initLoad();
              },
            ),
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: Spacing.spacing2XS),
                child: Column(
                  children: [
                    /*Padding(
                      padding: const EdgeInsets.only(top: Spacing.spacing2XS),
                      child: SearchBarWidget.controller(
                        cancelLabel: AppLocalizations.of(context)!.cancel,
                        controller: controller.searchBarController,
                        onCancelTap: () => controller.initLoad(),
                        onEditingComplete: () => controller.initLoad(),
                      ),
                    ),*/
                    if (state.loading)
                      const Expanded(
                        child: Center(
                          child: LoaderIndicator.sm(),
                        ),
                      ),
                    if (!state.loading)
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(top: Spacing.spacing2XS),
                          child: ListView.builder(
                            itemCount: controller.state.itemList.length,
                            itemBuilder: (BuildContext context, int index) {
                              final item = (controller.state.itemList)[index];

                              return GestureDetector(
                                onTap: () async {
                                  await GetIt.I.get<INavigationService>().routeOrderDetails(
                                        OrderDetailsPageArgs(
                                          orderUuid: item.uuid,
                                          isComplete: item.isComplete != 0,
                                          orderName: item.name,
                                        ),
                                      );

                                  controller.initLoad();
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: Sizing.size3XS,
                                  ),
                                  child: OrderItemWidget(
                                    onCloseTap: () => controller.remove(index),
                                    model: OrderItemModel(
                                      name: item.name,
                                      canRemove: item.isComplete == 0,
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
