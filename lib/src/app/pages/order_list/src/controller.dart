part of '../index.dart';

class OrderPageBlocController extends Bloc<AOrderListPageEvent, OrderListPageStateModel> {
  OrderPageBlocController({
    required this.args,
  }) : super(
          const OrderListPageStateModel(
            loading: true,
            itemList: [],
          ),
        ) {
    on<OrderListPageInitLoadEvent>(_onInitLoadEvent);
    on<OrderListPageSearchEvent>(_onSearchEvent);
    on<OrderListPageRemoveEvent>(_onRemoveEvent);

    initLoad();

    _timer = Timer.periodic(
      const Duration(
        seconds: 1,
      ),
      (timer) {
        if (_prevQuery != searchBarController.query) {
          if (searchBarController.query.isNotEmpty) {
            searchLoad(searchBarController.query);
          }

          _prevQuery = searchBarController.query;
        }
      },
    );
  }

  final SearchBarController searchBarController = SearchBarController(
    model: const SearchBarModel(text: ''),
  );

  late final Timer _timer;

  @override
  Future<void> close() {
    _timer.cancel();

    return super.close();
  }

  void initLoad() {
    add(
      OrderListPageInitLoadEvent(),
    );
  }

  String _prevQuery = '';

  void searchLoad(String query) {
    add(
      OrderListPageSearchEvent(
        query: query,
      ),
    );
  }

  void remove(int index) {
    add(
      OrderListPageRemoveEvent(
        index: index,
      ),
    );
  }

  final OrderListPageArgs args;

  void _onInitLoadEvent(
    OrderListPageInitLoadEvent event,
    Emitter<OrderListPageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    final result = await GetIt.instance.get<IAppRepository>().getOrderList();

    emit.call(
      state.copyWith(
        itemList: result,
        loading: false,
      ),
    );
  }

  void _onSearchEvent(
    OrderListPageSearchEvent event,
    Emitter<OrderListPageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    final result = await GetIt.instance.get<IAppRepository>().getOrderList();

    emit.call(
      state.copyWith(
        itemList: result,
        loading: false,
      ),
    );
  }

  void _onRemoveEvent(
    OrderListPageRemoveEvent event,
    Emitter<OrderListPageStateModel> emit,
  ) async {
    final item = state.itemList[event.index];

    final result = await GetIt.instance.get<IAppRepository>().removeOrder(item.uuid);

    if (!result) {
      return;
    }

    final list = List<OrderPlaceViewData>.generate(
      state.itemList.length,
      (index) => state.itemList[index],
    );

    list.removeAt(event.index);

    emit.call(
      state.copyWith(
        itemList: list,
      ),
    );
  }
}
