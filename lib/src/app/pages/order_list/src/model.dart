import 'package:app/src/data/repositories/local_storage/database/database.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class OrderListPageStateModel with _$OrderListPageStateModel {
  const factory OrderListPageStateModel({
    required List<OrderPlaceViewData> itemList,
    required bool loading,
  }) = _OrderListPageStateModel;
}
