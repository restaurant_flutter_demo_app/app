part of '../index.dart';

class OrderPageBlocController extends Bloc<AOrderCreatePageEvent, OrderCreatePageStateModel> {
  OrderPageBlocController()
      : super(
          const OrderCreatePageStateModel(
            loading: true,
            itemList: [],
          ),
        ) {
    on<OrderCreatePageInitLoadEvent>(_onInitLoadEvent);
    on<OrderCreatePageCreateEvent>(_onCreateEvent);

    initLoad();
  }

  void initLoad() {
    add(
      OrderCreatePageInitLoadEvent(),
    );
  }

  void createOrder(int index) {
    add(
      OrderCreatePageCreateEvent(index: index),
    );
  }

  void _onInitLoadEvent(
    OrderCreatePageInitLoadEvent event,
    Emitter<OrderCreatePageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    final result = await GetIt.instance.get<IAppRepository>().getPlaceList();

    emit.call(
      state.copyWith(
        itemList: result,
        loading: false,
      ),
    );
  }

  void _onCreateEvent(
    OrderCreatePageCreateEvent event,
    Emitter<OrderCreatePageStateModel> emit,
  ) async {
    final item = state.itemList[event.index];

    final result = await GetIt.instance.get<IAppRepository>().insertOrder(item.uuid);

    GetIt.I.get<INavigationService>().routeOrderCreate(
          OrderDetailsPageArgs(
            orderUuid: result,
            isComplete: false,
            orderName: item.name,
          ),
        );
  }
}
