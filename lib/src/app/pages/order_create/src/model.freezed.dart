// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$OrderCreatePageStateModel {
  List<PlaceData> get itemList => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $OrderCreatePageStateModelCopyWith<OrderCreatePageStateModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrderCreatePageStateModelCopyWith<$Res> {
  factory $OrderCreatePageStateModelCopyWith(OrderCreatePageStateModel value,
          $Res Function(OrderCreatePageStateModel) then) =
      _$OrderCreatePageStateModelCopyWithImpl<$Res, OrderCreatePageStateModel>;
  @useResult
  $Res call({List<PlaceData> itemList, bool loading});
}

/// @nodoc
class _$OrderCreatePageStateModelCopyWithImpl<$Res,
        $Val extends OrderCreatePageStateModel>
    implements $OrderCreatePageStateModelCopyWith<$Res> {
  _$OrderCreatePageStateModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? itemList = null,
    Object? loading = null,
  }) {
    return _then(_value.copyWith(
      itemList: null == itemList
          ? _value.itemList
          : itemList // ignore: cast_nullable_to_non_nullable
              as List<PlaceData>,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$OrderCreatePageStateModelImplCopyWith<$Res>
    implements $OrderCreatePageStateModelCopyWith<$Res> {
  factory _$$OrderCreatePageStateModelImplCopyWith(
          _$OrderCreatePageStateModelImpl value,
          $Res Function(_$OrderCreatePageStateModelImpl) then) =
      __$$OrderCreatePageStateModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<PlaceData> itemList, bool loading});
}

/// @nodoc
class __$$OrderCreatePageStateModelImplCopyWithImpl<$Res>
    extends _$OrderCreatePageStateModelCopyWithImpl<$Res,
        _$OrderCreatePageStateModelImpl>
    implements _$$OrderCreatePageStateModelImplCopyWith<$Res> {
  __$$OrderCreatePageStateModelImplCopyWithImpl(
      _$OrderCreatePageStateModelImpl _value,
      $Res Function(_$OrderCreatePageStateModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? itemList = null,
    Object? loading = null,
  }) {
    return _then(_$OrderCreatePageStateModelImpl(
      itemList: null == itemList
          ? _value._itemList
          : itemList // ignore: cast_nullable_to_non_nullable
              as List<PlaceData>,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$OrderCreatePageStateModelImpl implements _OrderCreatePageStateModel {
  const _$OrderCreatePageStateModelImpl(
      {required final List<PlaceData> itemList, required this.loading})
      : _itemList = itemList;

  final List<PlaceData> _itemList;
  @override
  List<PlaceData> get itemList {
    if (_itemList is EqualUnmodifiableListView) return _itemList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemList);
  }

  @override
  final bool loading;

  @override
  String toString() {
    return 'OrderCreatePageStateModel(itemList: $itemList, loading: $loading)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OrderCreatePageStateModelImpl &&
            const DeepCollectionEquality().equals(other._itemList, _itemList) &&
            (identical(other.loading, loading) || other.loading == loading));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_itemList), loading);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$OrderCreatePageStateModelImplCopyWith<_$OrderCreatePageStateModelImpl>
      get copyWith => __$$OrderCreatePageStateModelImplCopyWithImpl<
          _$OrderCreatePageStateModelImpl>(this, _$identity);
}

abstract class _OrderCreatePageStateModel implements OrderCreatePageStateModel {
  const factory _OrderCreatePageStateModel(
      {required final List<PlaceData> itemList,
      required final bool loading}) = _$OrderCreatePageStateModelImpl;

  @override
  List<PlaceData> get itemList;
  @override
  bool get loading;
  @override
  @JsonKey(ignore: true)
  _$$OrderCreatePageStateModelImplCopyWith<_$OrderCreatePageStateModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
