part of '../index.dart';

abstract class AOrderCreatePageEvent {}

final class OrderCreatePageInitLoadEvent extends AOrderCreatePageEvent {
  OrderCreatePageInitLoadEvent();
}

final class OrderCreatePageCreateEvent extends AOrderCreatePageEvent {
  OrderCreatePageCreateEvent({
    required this.index,
  });

  final int index;
}
