import 'package:app/src/data/repositories/local_storage/database/database.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class OrderCreatePageStateModel with _$OrderCreatePageStateModel {
  const factory OrderCreatePageStateModel({
    required List<PlaceData> itemList,
    required bool loading,
  }) = _OrderCreatePageStateModel;
}
