part of '../index.dart';

class OrderCreatePageView extends StatefulWidget {
  const OrderCreatePageView({
    super.key,
  }) : super();

  @override
  State<OrderCreatePageView> createState() => _OrderCreatePageViewState();
}

class _OrderCreatePageViewState extends State<OrderCreatePageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    //context.read<OrderPageBlocController>().close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => OrderPageBlocController(),
      child: BlocBuilder<OrderPageBlocController, OrderCreatePageStateModel>(
        builder: (
          context,
          state,
        ) {
          final OrderPageBlocController controller = context.read<OrderPageBlocController>();

          return Scaffold(
            appBar: OrderCreateAppBar(
              onBackTap: () => Navigator.of(context).pop(),
              title: AppLocalizations.of(context)!.order_create,
            ),
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: Spacing.spacing2XS),
                child: Column(
                  children: [
                    if (state.loading)
                      const Expanded(
                        child: Center(
                          child: LoaderIndicator.sm(),
                        ),
                      ),
                    if (!state.loading)
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(top: Spacing.spacing2XS),
                          child: ListView.builder(
                            itemCount: controller.state.itemList.length,
                            itemBuilder: (BuildContext context, int index) {
                              final item = (controller.state.itemList)[index];

                              return GestureDetector(
                                onTap: () {
                                  controller.createOrder(index);
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: Sizing.size3XS,
                                  ),
                                  child: PlaceItemWidget(
                                    name: item.name,
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
