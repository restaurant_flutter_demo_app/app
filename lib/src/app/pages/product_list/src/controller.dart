part of '../index.dart';

class ProductPageBlocController extends Bloc<AProductListPageEvent, ProductListPageStateModel> {
  ProductPageBlocController({
    required this.args,
  }) : super(
          ProductListPageStateModel(
            loading: true,
            itemList: [],
            selectedUidsList: [],
            disableUidsList: args.selectedUuidList,
          ),
        ) {
    on<ProductListPageInitLoadEvent>(_onInitLoadEvent);
    on<ProductListPageSearchEvent>(_onSearchEvent);
    on<ProductListPageSelectItemEvent>(_onSelectItemEvent);
    on<ProductListPageSaveEvent>(_onSaveEvent);

    initLoad();

    _timer = Timer.periodic(
      const Duration(
        seconds: 1,
      ),
      (timer) {
        if (_prevQuery != searchBarController.query) {
          if (searchBarController.query.isNotEmpty) {
            searchLoad(searchBarController.query);
          }

          _prevQuery = searchBarController.query;
        }
      },
    );
  }

  final ProductListPageArgs args;

  final SearchBarController searchBarController = SearchBarController(
    model: const SearchBarModel(text: ''),
  );

  late final Timer _timer;

  @override
  Future<void> close() {
    _timer.cancel();

    return super.close();
  }

  bool checkDisable(String uuid) {
    return state.disableUidsList.contains(uuid);
  }

  bool checkSelect(String uuid) {
    return state.selectedUidsList.contains(uuid);
  }

  void initLoad() => add(
        ProductListPageInitLoadEvent(),
      );

  String _prevQuery = '';

  void searchLoad(String query) => add(
        ProductListPageSearchEvent(
          query: query,
        ),
      );

  void selectItem(int index) => add(
        ProductListPageSelectItemEvent(index: index),
      );

  void save() => add(
        ProductListPageSaveEvent(),
      );

  void _onInitLoadEvent(
    ProductListPageInitLoadEvent event,
    Emitter<ProductListPageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    final result = await GetIt.instance.get<IAppRepository>().getProductList();

    emit.call(
      state.copyWith(
        itemList: result,
        loading: false,
      ),
    );
  }

  void _onSearchEvent(
    ProductListPageSearchEvent event,
    Emitter<ProductListPageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    final result = await GetIt.instance.get<IAppRepository>().getProductList();

    emit.call(
      state.copyWith(
        itemList: result,
        loading: false,
      ),
    );
  }

  void _onSelectItemEvent(
    ProductListPageSelectItemEvent event,
    Emitter<ProductListPageStateModel> emit,
  ) async {
    final item = state.itemList[event.index];

    if (checkDisable(item.uuid)) {
      return;
    }

    final selected = List<String>.generate(
      state.selectedUidsList.length,
      (index) => state.selectedUidsList[index],
    );

    if (checkSelect(item.uuid)) {
      selected.remove(item.uuid);
    } else {
      selected.add(item.uuid);
    }

    emit.call(
      state.copyWith(
        selectedUidsList: selected,
        loading: false,
      ),
    );
  }

  void _onSaveEvent(
    ProductListPageSaveEvent event,
    Emitter<ProductListPageStateModel> emit,
  ) async {
    await GetIt.instance.get<IAppRepository>().insertOrderProducts(
          args.orderUuid,
          state.selectedUidsList,
        );
  }
}
