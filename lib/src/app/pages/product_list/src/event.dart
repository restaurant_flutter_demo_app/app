part of '../index.dart';

abstract class AProductListPageEvent {}

final class ProductListPageInitLoadEvent extends AProductListPageEvent {
  ProductListPageInitLoadEvent();
}

final class ProductListPageSearchEvent extends AProductListPageEvent {
  ProductListPageSearchEvent({
    required this.query,
  });

  final String query;
}

final class ProductListPageSelectItemEvent extends AProductListPageEvent {
  ProductListPageSelectItemEvent({
    required this.index,
  });

  final int index;
}

final class ProductListPageSaveEvent extends AProductListPageEvent {
  ProductListPageSaveEvent();
}
