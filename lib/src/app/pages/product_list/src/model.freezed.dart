// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ProductListPageStateModel {
  List<ProductViewData> get itemList => throw _privateConstructorUsedError;
  List<String> get selectedUidsList => throw _privateConstructorUsedError;
  List<String> get disableUidsList => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProductListPageStateModelCopyWith<ProductListPageStateModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductListPageStateModelCopyWith<$Res> {
  factory $ProductListPageStateModelCopyWith(ProductListPageStateModel value,
          $Res Function(ProductListPageStateModel) then) =
      _$ProductListPageStateModelCopyWithImpl<$Res, ProductListPageStateModel>;
  @useResult
  $Res call(
      {List<ProductViewData> itemList,
      List<String> selectedUidsList,
      List<String> disableUidsList,
      bool loading});
}

/// @nodoc
class _$ProductListPageStateModelCopyWithImpl<$Res,
        $Val extends ProductListPageStateModel>
    implements $ProductListPageStateModelCopyWith<$Res> {
  _$ProductListPageStateModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? itemList = null,
    Object? selectedUidsList = null,
    Object? disableUidsList = null,
    Object? loading = null,
  }) {
    return _then(_value.copyWith(
      itemList: null == itemList
          ? _value.itemList
          : itemList // ignore: cast_nullable_to_non_nullable
              as List<ProductViewData>,
      selectedUidsList: null == selectedUidsList
          ? _value.selectedUidsList
          : selectedUidsList // ignore: cast_nullable_to_non_nullable
              as List<String>,
      disableUidsList: null == disableUidsList
          ? _value.disableUidsList
          : disableUidsList // ignore: cast_nullable_to_non_nullable
              as List<String>,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ProductListPageStateModelImplCopyWith<$Res>
    implements $ProductListPageStateModelCopyWith<$Res> {
  factory _$$ProductListPageStateModelImplCopyWith(
          _$ProductListPageStateModelImpl value,
          $Res Function(_$ProductListPageStateModelImpl) then) =
      __$$ProductListPageStateModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<ProductViewData> itemList,
      List<String> selectedUidsList,
      List<String> disableUidsList,
      bool loading});
}

/// @nodoc
class __$$ProductListPageStateModelImplCopyWithImpl<$Res>
    extends _$ProductListPageStateModelCopyWithImpl<$Res,
        _$ProductListPageStateModelImpl>
    implements _$$ProductListPageStateModelImplCopyWith<$Res> {
  __$$ProductListPageStateModelImplCopyWithImpl(
      _$ProductListPageStateModelImpl _value,
      $Res Function(_$ProductListPageStateModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? itemList = null,
    Object? selectedUidsList = null,
    Object? disableUidsList = null,
    Object? loading = null,
  }) {
    return _then(_$ProductListPageStateModelImpl(
      itemList: null == itemList
          ? _value._itemList
          : itemList // ignore: cast_nullable_to_non_nullable
              as List<ProductViewData>,
      selectedUidsList: null == selectedUidsList
          ? _value._selectedUidsList
          : selectedUidsList // ignore: cast_nullable_to_non_nullable
              as List<String>,
      disableUidsList: null == disableUidsList
          ? _value._disableUidsList
          : disableUidsList // ignore: cast_nullable_to_non_nullable
              as List<String>,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$ProductListPageStateModelImpl implements _ProductListPageStateModel {
  const _$ProductListPageStateModelImpl(
      {required final List<ProductViewData> itemList,
      required final List<String> selectedUidsList,
      required final List<String> disableUidsList,
      required this.loading})
      : _itemList = itemList,
        _selectedUidsList = selectedUidsList,
        _disableUidsList = disableUidsList;

  final List<ProductViewData> _itemList;
  @override
  List<ProductViewData> get itemList {
    if (_itemList is EqualUnmodifiableListView) return _itemList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemList);
  }

  final List<String> _selectedUidsList;
  @override
  List<String> get selectedUidsList {
    if (_selectedUidsList is EqualUnmodifiableListView)
      return _selectedUidsList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_selectedUidsList);
  }

  final List<String> _disableUidsList;
  @override
  List<String> get disableUidsList {
    if (_disableUidsList is EqualUnmodifiableListView) return _disableUidsList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_disableUidsList);
  }

  @override
  final bool loading;

  @override
  String toString() {
    return 'ProductListPageStateModel(itemList: $itemList, selectedUidsList: $selectedUidsList, disableUidsList: $disableUidsList, loading: $loading)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductListPageStateModelImpl &&
            const DeepCollectionEquality().equals(other._itemList, _itemList) &&
            const DeepCollectionEquality()
                .equals(other._selectedUidsList, _selectedUidsList) &&
            const DeepCollectionEquality()
                .equals(other._disableUidsList, _disableUidsList) &&
            (identical(other.loading, loading) || other.loading == loading));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_itemList),
      const DeepCollectionEquality().hash(_selectedUidsList),
      const DeepCollectionEquality().hash(_disableUidsList),
      loading);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProductListPageStateModelImplCopyWith<_$ProductListPageStateModelImpl>
      get copyWith => __$$ProductListPageStateModelImplCopyWithImpl<
          _$ProductListPageStateModelImpl>(this, _$identity);
}

abstract class _ProductListPageStateModel implements ProductListPageStateModel {
  const factory _ProductListPageStateModel(
      {required final List<ProductViewData> itemList,
      required final List<String> selectedUidsList,
      required final List<String> disableUidsList,
      required final bool loading}) = _$ProductListPageStateModelImpl;

  @override
  List<ProductViewData> get itemList;
  @override
  List<String> get selectedUidsList;
  @override
  List<String> get disableUidsList;
  @override
  bool get loading;
  @override
  @JsonKey(ignore: true)
  _$$ProductListPageStateModelImplCopyWith<_$ProductListPageStateModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
