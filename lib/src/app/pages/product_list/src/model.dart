import 'package:app/src/data/repositories/local_storage/database/database.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class ProductListPageStateModel with _$ProductListPageStateModel {
  const factory ProductListPageStateModel({
    required List<ProductViewData> itemList,
    required List<String> selectedUidsList,
    required List<String> disableUidsList,
    required bool loading,
  }) = _ProductListPageStateModel;
}
