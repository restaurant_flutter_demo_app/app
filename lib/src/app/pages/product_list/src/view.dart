part of '../index.dart';

class ProductListPageArgs {
  ProductListPageArgs({
    required this.orderUuid,
    required this.selectedUuidList,
  });

  final String orderUuid;
  final List<String> selectedUuidList;
}

class ProductListPageView extends StatefulWidget {
  const ProductListPageView({
    required this.args,
    super.key,
  }) : super();

  final ProductListPageArgs args;

  @override
  State<ProductListPageView> createState() => _ProductListPageViewState();
}

class _ProductListPageViewState extends State<ProductListPageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    //context.read<ProductPageBlocController>().close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => ProductPageBlocController(args: widget.args),
      child: BlocBuilder<ProductPageBlocController, ProductListPageStateModel>(
        builder: (
          context,
          state,
        ) {
          final ProductPageBlocController controller = context.read<ProductPageBlocController>();

          return Scaffold(
            appBar: ProductAppBar(
              onBackTap: () => Navigator.of(context).pop(),
              onSaveTap: () {
                controller.save();

                Navigator.of(context).pop();
              },
              title: AppLocalizations.of(context)!.products,
            ),
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: Spacing.spacing2XS),
                child: Column(
                  children: [
                    /*Padding(
                      padding: const EdgeInsets.only(top: Spacing.spacing2XS),
                      child: SearchBarWidget.controller(
                        cancelLabel: AppLocalizations.of(context)!.cancel,
                        controller: controller.searchBarController,
                        onCancelTap: () => controller.initLoad(),
                        onEditingComplete: () => controller.initLoad(),
                      ),
                    ),*/
                    if (state.loading)
                      const Expanded(
                        child: Center(
                          child: LoaderIndicator.sm(),
                        ),
                      ),
                    if (!state.loading)
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(top: Spacing.spacing2XS),
                          child: ListView.builder(
                            itemCount: controller.state.itemList.length * 2,
                            itemBuilder: (BuildContext context, int index) {
                              final int itemIndex = index ~/ 2;

                              final item = (controller.state.itemList)[itemIndex];

                              if (index.isEven) {
                                if (itemIndex == 0) {
                                  return SeparatorItemWidget(name: item.productTypeName ?? '');
                                } else {
                                  final itemPrev = (controller.state.itemList)[itemIndex - 1];

                                  if (item.productTypeUuid != itemPrev.productTypeUuid) {
                                    return SeparatorItemWidget(name: item.productTypeName ?? '');
                                  } else {
                                    return const SizedBox.shrink();
                                  }
                                }
                              }

                              final ProductItemWidgetState state;

                              if (controller.checkDisable(item.uuid)) {
                                state = ProductItemWidgetState.disable;
                              } else if (controller.checkSelect(item.uuid)) {
                                state = ProductItemWidgetState.selected;
                              } else {
                                state = ProductItemWidgetState.unselected;
                              }

                              var formatter = NumberFormat('00');

                              return GestureDetector(
                                onTap: () {
                                  controller.selectItem(itemIndex);
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: Spacing.spacing2XS),
                                  child: ProductItemWidget(
                                    name: item.name,
                                    state: state,
                                    priceLabel: AppLocalizations.of(context)!.price,
                                    price: '${item.price}.${formatter.format(item.priceCoins)}',
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
