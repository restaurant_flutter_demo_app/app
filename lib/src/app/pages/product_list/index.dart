import 'dart:async';

import 'package:app/src/data/repositories/repository/i_app_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:ui_kit/ui_kit.dart';

import 'index.dart';

export 'src/model.dart';

part 'src/controller.dart';
part 'src/event.dart';
part 'src/view.dart';
