part of '../index.dart';

class OrderDetailsPageArgs {
  OrderDetailsPageArgs({
    required this.orderUuid,
    required this.isComplete,
    required this.orderName,
  });

  final String orderUuid;
  final String orderName;
  final bool isComplete;
}

class OrderDetailsPageView extends StatefulWidget {
  const OrderDetailsPageView({
    required this.args,
    super.key,
  }) : super();

  final OrderDetailsPageArgs args;

  @override
  State<OrderDetailsPageView> createState() => _OrderDetailsPageViewState();
}

class _OrderDetailsPageViewState extends State<OrderDetailsPageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => OrderDetailsPageBlocController(args: widget.args),
      child: BlocBuilder<OrderDetailsPageBlocController, OrderDetailsPageStateModel>(
        builder: (
          context,
          state,
        ) {
          final OrderDetailsPageBlocController controller = context.read<OrderDetailsPageBlocController>();

          return Scaffold(
            appBar: OrderAppBar(
              onBackTap: () => Navigator.of(context).pop(),
              onAddTap: () async {
                await GetIt.I.get<INavigationService>().routeProductList(
                      ProductListPageArgs(
                        orderUuid: widget.args.orderUuid,
                        selectedUuidList: controller.selectedUuidList,
                      ),
                    );

                controller.initLoad();
              },
              title: controller.args.orderName,
            ),
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: Spacing.spacing2XS),
                child: Column(
                  children: [
                    if (state.loading)
                      const Expanded(
                        child: Center(
                          child: LoaderIndicator.sm(),
                        ),
                      ),
                    if (!state.loading)
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(top: Spacing.spacing2XS),
                          child: ListView.builder(
                            itemCount: controller.state.itemList.length,
                            itemBuilder: (BuildContext context, int index) {
                              final item = (controller.state.itemList)[index];

                              return GestureDetector(
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: Sizing.size3XS,
                                  ),
                                  child: OrderProductItemWidget(
                                    onAddTap: () => controller.addCount(index),
                                    onRemoveTap: () => controller.removeCount(index),
                                    onCloseTap: () => controller.removeItem(index),
                                    model: OrderProductItemModel(
                                      name: item.name,
                                      count: item.count,
                                      canUpdate: !widget.args.isComplete,
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
