part of '../index.dart';

abstract class AOrderDetailsPageEvent {}

final class OrderDetailsPageInitLoadEvent extends AOrderDetailsPageEvent {
  OrderDetailsPageInitLoadEvent();
}

final class OrderDetailsPageAddCountEvent extends AOrderDetailsPageEvent {
  OrderDetailsPageAddCountEvent({
    required this.index,
  });

  final int index;
}

final class OrderDetailsPageRemoveCountEvent extends AOrderDetailsPageEvent {
  OrderDetailsPageRemoveCountEvent({
    required this.index,
  });

  final int index;
}

final class OrderDetailsIRemoveItemEvent extends AOrderDetailsPageEvent {
  OrderDetailsIRemoveItemEvent({
    required this.index,
  });

  final int index;
}
