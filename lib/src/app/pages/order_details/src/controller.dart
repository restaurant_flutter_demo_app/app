part of '../index.dart';

class OrderDetailsPageBlocController extends Bloc<AOrderDetailsPageEvent, OrderDetailsPageStateModel> {
  OrderDetailsPageBlocController({
    required this.args,
  }) : super(
          const OrderDetailsPageStateModel(
            loading: true,
            itemList: [],
          ),
        ) {
    on<OrderDetailsPageInitLoadEvent>(_onInitLoadEvent);
    on<OrderDetailsPageAddCountEvent>(_onAddCountEvent);
    on<OrderDetailsPageRemoveCountEvent>(_onRemoveCountEvent);
    on<OrderDetailsIRemoveItemEvent>(_onRemoveItemEvent);

    initLoad();
  }

  final OrderDetailsPageArgs args;

  void initLoad() => add(
        OrderDetailsPageInitLoadEvent(),
      );

  void addCount(index) => add(
        OrderDetailsPageAddCountEvent(index: index),
      );

  void removeCount(index) => add(
        OrderDetailsPageRemoveCountEvent(index: index),
      );

  void removeItem(index) => add(
        OrderDetailsIRemoveItemEvent(index: index),
      );

  List<String> get selectedUuidList => List<String>.generate(
        state.itemList.length,
        (index) => state.itemList[index].productUuid,
      );

  void _onInitLoadEvent(
    OrderDetailsPageInitLoadEvent event,
    Emitter<OrderDetailsPageStateModel> emit,
  ) async {
    emit.call(
      state.copyWith(
        loading: true,
      ),
    );

    final result = await GetIt.instance.get<IAppRepository>().getOrder(args.orderUuid);

    emit.call(
      state.copyWith(
        itemList: result,
        loading: false,
      ),
    );
  }

  void _onAddCountEvent(
    OrderDetailsPageAddCountEvent event,
    Emitter<OrderDetailsPageStateModel> emit,
  ) async {
    final item = state.itemList[event.index];

    final count = item.count + 1;

    await _updateCount(item, count, emit);
  }

  void _onRemoveCountEvent(
    OrderDetailsPageRemoveCountEvent event,
    Emitter<OrderDetailsPageStateModel> emit,
  ) async {
    final item = state.itemList[event.index];

    if (item.count == 1) {
      return;
    }

    final count = item.count - 1;

    await _updateCount(item, count, emit);
  }

  void _onRemoveItemEvent(
    OrderDetailsIRemoveItemEvent event,
    Emitter<OrderDetailsPageStateModel> emit,
  ) async {
    final item = state.itemList[event.index];

    final result = await GetIt.instance.get<IAppRepository>().removeOrderProduct(item.uuid);

    if (result) {
      final list = List<OrderProductViewData>.generate(
        state.itemList.length,
        (index) => state.itemList[index],
      );

      list.removeAt(event.index);

      emit.call(
        state.copyWith(
          itemList: list,
        ),
      );
    }
  }

  Future<void> _updateCount(
    OrderProductViewData item,
    int count,
    Emitter<OrderDetailsPageStateModel> emit,
  ) async {
    final result = await GetIt.instance.get<IAppRepository>().updateOrderProduct(
          item.uuid,
          count,
        );

    if (result) {
      final list = List<OrderProductViewData>.generate(
        state.itemList.length,
        (int index) {
          if (state.itemList[index].uuid == item.uuid) {
            return state.itemList[index].copyWith(count: count);
          }

          return state.itemList[index];
        },
      );

      emit.call(
        state.copyWith(
          itemList: list,
          loading: false,
        ),
      );
    }
  }
}
