import 'package:app/src/data/repositories/local_storage/database/database.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class OrderDetailsPageStateModel with _$OrderDetailsPageStateModel {
  const factory OrderDetailsPageStateModel({
    required List<OrderProductViewData> itemList,
    required bool loading,
  }) = _OrderDetailsPageStateModel;
}
