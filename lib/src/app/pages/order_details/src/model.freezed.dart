// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$OrderDetailsPageStateModel {
  List<OrderProductViewData> get itemList => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $OrderDetailsPageStateModelCopyWith<OrderDetailsPageStateModel>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrderDetailsPageStateModelCopyWith<$Res> {
  factory $OrderDetailsPageStateModelCopyWith(OrderDetailsPageStateModel value,
          $Res Function(OrderDetailsPageStateModel) then) =
      _$OrderDetailsPageStateModelCopyWithImpl<$Res,
          OrderDetailsPageStateModel>;
  @useResult
  $Res call({List<OrderProductViewData> itemList, bool loading});
}

/// @nodoc
class _$OrderDetailsPageStateModelCopyWithImpl<$Res,
        $Val extends OrderDetailsPageStateModel>
    implements $OrderDetailsPageStateModelCopyWith<$Res> {
  _$OrderDetailsPageStateModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? itemList = null,
    Object? loading = null,
  }) {
    return _then(_value.copyWith(
      itemList: null == itemList
          ? _value.itemList
          : itemList // ignore: cast_nullable_to_non_nullable
              as List<OrderProductViewData>,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$OrderDetailsPageStateModelImplCopyWith<$Res>
    implements $OrderDetailsPageStateModelCopyWith<$Res> {
  factory _$$OrderDetailsPageStateModelImplCopyWith(
          _$OrderDetailsPageStateModelImpl value,
          $Res Function(_$OrderDetailsPageStateModelImpl) then) =
      __$$OrderDetailsPageStateModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<OrderProductViewData> itemList, bool loading});
}

/// @nodoc
class __$$OrderDetailsPageStateModelImplCopyWithImpl<$Res>
    extends _$OrderDetailsPageStateModelCopyWithImpl<$Res,
        _$OrderDetailsPageStateModelImpl>
    implements _$$OrderDetailsPageStateModelImplCopyWith<$Res> {
  __$$OrderDetailsPageStateModelImplCopyWithImpl(
      _$OrderDetailsPageStateModelImpl _value,
      $Res Function(_$OrderDetailsPageStateModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? itemList = null,
    Object? loading = null,
  }) {
    return _then(_$OrderDetailsPageStateModelImpl(
      itemList: null == itemList
          ? _value._itemList
          : itemList // ignore: cast_nullable_to_non_nullable
              as List<OrderProductViewData>,
      loading: null == loading
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$OrderDetailsPageStateModelImpl implements _OrderDetailsPageStateModel {
  const _$OrderDetailsPageStateModelImpl(
      {required final List<OrderProductViewData> itemList,
      required this.loading})
      : _itemList = itemList;

  final List<OrderProductViewData> _itemList;
  @override
  List<OrderProductViewData> get itemList {
    if (_itemList is EqualUnmodifiableListView) return _itemList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemList);
  }

  @override
  final bool loading;

  @override
  String toString() {
    return 'OrderDetailsPageStateModel(itemList: $itemList, loading: $loading)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OrderDetailsPageStateModelImpl &&
            const DeepCollectionEquality().equals(other._itemList, _itemList) &&
            (identical(other.loading, loading) || other.loading == loading));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_itemList), loading);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$OrderDetailsPageStateModelImplCopyWith<_$OrderDetailsPageStateModelImpl>
      get copyWith => __$$OrderDetailsPageStateModelImplCopyWithImpl<
          _$OrderDetailsPageStateModelImpl>(this, _$identity);
}

abstract class _OrderDetailsPageStateModel
    implements OrderDetailsPageStateModel {
  const factory _OrderDetailsPageStateModel(
      {required final List<OrderProductViewData> itemList,
      required final bool loading}) = _$OrderDetailsPageStateModelImpl;

  @override
  List<OrderProductViewData> get itemList;
  @override
  bool get loading;
  @override
  @JsonKey(ignore: true)
  _$$OrderDetailsPageStateModelImplCopyWith<_$OrderDetailsPageStateModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
