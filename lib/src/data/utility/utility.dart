import 'package:flutter/services.dart';

class DataUtility {
  static Future<String> readStringFile({
    required String path,
  }) async {
    final result = await rootBundle.loadString(
      path,
    );

    return result;
  }
}
