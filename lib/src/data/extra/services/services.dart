import 'package:app/src/app/extra/util/navigation.dart';
import 'package:app/src/data/repositories/repository/i_app_repository.dart';
import 'package:app/src/data/repositories/repository/repository.dart';
import 'package:get_it/get_it.dart';

/// Is a smart move to make your Services intiialize before you run the Flutter app.
/// as you can control the execution flow (maybe you need to load some Theme configuration,
/// apiKey, language defined by the User... so load SettingService before running ApiService.
/// so GetMaterialApp() doesnt have to rebuild, and takes the values directly.

class Services {
  static void initServices() async {
    GetIt.I.registerSingleton<IAppRepository>(
      signalsReady: true,
      AppRepository(),
    );
  }

  static void initNavigationService() async => GetIt.I.registerSingleton<INavigationService>(
        NavigationService(),
      );
}
