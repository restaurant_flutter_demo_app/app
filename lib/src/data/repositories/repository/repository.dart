import 'dart:core';

import 'package:app/src/data/repositories/local_storage/database/database.dart' as db;
import 'package:app/src/data/repositories/repository/i_app_repository.dart';
import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';

part 'repository.order.dart';
part 'repository.order_product.dart';
part 'repository.place.dart';
part 'repository.product.dart';

class AppRepository implements IAppRepository {
  AppRepository();

  final storageDb = db.AppDatabase();

  /// PRODUCT
  @override
  Future<List<db.ProductViewData>> getProductList() => _getProductList();

  /// ORDER
  @override
  Future<List<db.OrderPlaceViewData>> getOrderList() => _getOrderList();

  @override
  Future<List<db.OrderProductViewData>> getOrder(String orderUuid) => _getOrder(orderUuid);

  @override
  Future<String> insertOrder(String placeUuid) => _insertOrder(placeUuid);

  @override
  Future<bool> removeOrder(String orderUuid) => _removeOrder(orderUuid);

  /// ORDER PRODUCT
  @override
  Future<bool> updateOrderProduct(String uuid, int? count) => _updateOrderProduct(uuid, count);

  @override
  Future<void> insertOrderProducts(String orderUuid, List<String> productUuidList) => _insertOrderProducts(
        orderUuid,
        productUuidList,
      );

  int get datetimeNowUtcMilliseconds {
    final dateTime = DateTime.timestamp();

    return dateTime.millisecondsSinceEpoch;
  }

  @override
  Future<bool> removeOrderProduct(String uuid) => _removeOrderProduct(uuid);

  @override
  Future<List<db.PlaceData>> getPlaceList() => _getPlaceList();
}
