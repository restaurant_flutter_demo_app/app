part of 'repository.dart';

extension Order on AppRepository {
  Future<List<db.OrderPlaceViewData>> _getOrderList() async {
    final result = await (storageDb.select(storageDb.orderPlaceView)
          ..orderBy(
            ([
              (u) => OrderingTerm(
                    expression: u.datetime,
                    mode: OrderingMode.desc,
                  ),
            ]),
          ))
        .get();

    return result;
  }

  Future<List<db.OrderProductViewData>> _getOrder(String orderUuid) async {
    final result = await (storageDb.select(storageDb.orderProductView)
          ..where((u) => u.orderUuid.equals(orderUuid))
          ..orderBy(
            ([
              (u) => OrderingTerm(
                    expression: u.datetime,
                    mode: OrderingMode.desc,
                  ),
            ]),
          ))
        .get();

    return result;
  }

  Future<String> _insertOrder(String placeUuid) async {
    final uuid = const Uuid().v4();

    await storageDb.orderPlace.insertOne(
      db.OrderPlaceData(
        uuid: uuid,
        datetime: datetimeNowUtcMilliseconds,
        placeUuid: placeUuid,
        isComplete: 0,
      ),
    );

    return uuid;
  }

  Future<bool> _removeOrder(String orderUuid) async {
    await storageDb.orderProduct.deleteWhere(
      (tbl) => tbl.orderUuid.equalsNullable(
        orderUuid,
      ),
    );

    return (await storageDb.orderPlace.deleteWhere(
          (tbl) => tbl.uuid.equalsNullable(
            orderUuid,
          ),
        )) >
        0;
  }
}
