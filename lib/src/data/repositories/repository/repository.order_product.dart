part of 'repository.dart';

extension OrderProduct on AppRepository {
  Future<bool> _updateOrderProduct(String uuid, int? count) async {
    final result = await (storageDb.update(storageDb.orderProduct)
              ..where(
                (u) => u.uuid.equals(uuid),
              ))
            .write(
          db.OrderProductCompanion(
            count: Value.absentIfNull(count),
          ),
        ) >
        0;

    return result;
  }

  Future<void> _insertOrderProducts(String orderUuid, List<String> productUuidList) async {
    await storageDb.orderProduct.insertAll(
      List<db.OrderProductData>.generate(
        productUuidList.length,
        (index) => db.OrderProductData(
          uuid: const Uuid().v4(),
          productUuid: productUuidList[index],
          orderUuid: orderUuid,
          datetime: datetimeNowUtcMilliseconds,
          count: 1,
        ),
      ),
    );

    return;
  }

  Future<bool> _removeOrderProduct(String uuid) async {
    return (await storageDb.orderProduct.deleteWhere(
          (tbl) => tbl.uuid.equalsNullable(
            uuid,
          ),
        )) >
        0;
  }
}
