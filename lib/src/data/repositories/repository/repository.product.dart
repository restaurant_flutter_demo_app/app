part of 'repository.dart';

extension Product on AppRepository {
  Future<List<db.ProductViewData>> _getProductList() async {
    final result = await (storageDb.select(storageDb.productView)
          ..orderBy(
            ([
              (u) => OrderingTerm(
                    expression: u.productTypeName,
                    mode: OrderingMode.asc,
                  ),
            ]),
          ))
        .get();

    return result;
  }
}
