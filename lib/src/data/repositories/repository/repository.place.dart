part of 'repository.dart';

extension Place on AppRepository {
  Future<List<db.PlaceData>> _getPlaceList() async {
    final result = await (storageDb.select(storageDb.place)
          ..orderBy(
            ([
              (u) => OrderingTerm(
                    expression: u.name,
                    mode: OrderingMode.asc,
                  ),
            ]),
          ))
        .get();

    return result;
  }
}
