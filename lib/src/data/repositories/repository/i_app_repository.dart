import 'package:app/src/data/repositories/local_storage/database/database.dart';

abstract class IAppRepository {
  Future<List<ProductViewData>> getProductList();
  Future<List<OrderPlaceViewData>> getOrderList();
  Future<List<PlaceData>> getPlaceList();
  Future<List<OrderProductViewData>> getOrder(String orderUuid);
  Future<String> insertOrder(String placeUuid);
  Future<bool> removeOrder(String orderUuid);
  Future<void> insertOrderProducts(String orderUuid, List<String> productUuidList);
  Future<bool> updateOrderProduct(String uuid, int? count);
  Future<bool> removeOrderProduct(String uuid);
}
