// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// ignore_for_file: type=lint
class OrderPlace extends Table with TableInfo<OrderPlace, OrderPlaceData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  OrderPlace(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'Uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL UNIQUE');
  static const VerificationMeta _datetimeMeta =
      const VerificationMeta('datetime');
  late final GeneratedColumn<int> datetime = GeneratedColumn<int>(
      'Datetime', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL');
  static const VerificationMeta _placeUuidMeta =
      const VerificationMeta('placeUuid');
  late final GeneratedColumn<String> placeUuid = GeneratedColumn<String>(
      'PlaceUuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL');
  static const VerificationMeta _isCompleteMeta =
      const VerificationMeta('isComplete');
  late final GeneratedColumn<int> isComplete = GeneratedColumn<int>(
      'IsComplete', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL');
  @override
  List<GeneratedColumn> get $columns => [uuid, datetime, placeUuid, isComplete];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'OrderPlace';
  @override
  VerificationContext validateIntegrity(Insertable<OrderPlaceData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('Uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['Uuid']!, _uuidMeta));
    } else if (isInserting) {
      context.missing(_uuidMeta);
    }
    if (data.containsKey('Datetime')) {
      context.handle(_datetimeMeta,
          datetime.isAcceptableOrUnknown(data['Datetime']!, _datetimeMeta));
    } else if (isInserting) {
      context.missing(_datetimeMeta);
    }
    if (data.containsKey('PlaceUuid')) {
      context.handle(_placeUuidMeta,
          placeUuid.isAcceptableOrUnknown(data['PlaceUuid']!, _placeUuidMeta));
    } else if (isInserting) {
      context.missing(_placeUuidMeta);
    }
    if (data.containsKey('IsComplete')) {
      context.handle(
          _isCompleteMeta,
          isComplete.isAcceptableOrUnknown(
              data['IsComplete']!, _isCompleteMeta));
    } else if (isInserting) {
      context.missing(_isCompleteMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  OrderPlaceData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return OrderPlaceData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Uuid'])!,
      datetime: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}Datetime'])!,
      placeUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}PlaceUuid'])!,
      isComplete: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}IsComplete'])!,
    );
  }

  @override
  OrderPlace createAlias(String alias) {
    return OrderPlace(attachedDatabase, alias);
  }

  @override
  List<String> get customConstraints => const ['PRIMARY KEY(Uuid)'];
  @override
  bool get dontWriteConstraints => true;
}

class OrderPlaceData extends DataClass implements Insertable<OrderPlaceData> {
  final String uuid;
  final int datetime;
  final String placeUuid;
  final int isComplete;
  const OrderPlaceData(
      {required this.uuid,
      required this.datetime,
      required this.placeUuid,
      required this.isComplete});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['Uuid'] = Variable<String>(uuid);
    map['Datetime'] = Variable<int>(datetime);
    map['PlaceUuid'] = Variable<String>(placeUuid);
    map['IsComplete'] = Variable<int>(isComplete);
    return map;
  }

  OrderPlaceCompanion toCompanion(bool nullToAbsent) {
    return OrderPlaceCompanion(
      uuid: Value(uuid),
      datetime: Value(datetime),
      placeUuid: Value(placeUuid),
      isComplete: Value(isComplete),
    );
  }

  factory OrderPlaceData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return OrderPlaceData(
      uuid: serializer.fromJson<String>(json['Uuid']),
      datetime: serializer.fromJson<int>(json['Datetime']),
      placeUuid: serializer.fromJson<String>(json['PlaceUuid']),
      isComplete: serializer.fromJson<int>(json['IsComplete']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'Uuid': serializer.toJson<String>(uuid),
      'Datetime': serializer.toJson<int>(datetime),
      'PlaceUuid': serializer.toJson<String>(placeUuid),
      'IsComplete': serializer.toJson<int>(isComplete),
    };
  }

  OrderPlaceData copyWith(
          {String? uuid, int? datetime, String? placeUuid, int? isComplete}) =>
      OrderPlaceData(
        uuid: uuid ?? this.uuid,
        datetime: datetime ?? this.datetime,
        placeUuid: placeUuid ?? this.placeUuid,
        isComplete: isComplete ?? this.isComplete,
      );
  @override
  String toString() {
    return (StringBuffer('OrderPlaceData(')
          ..write('uuid: $uuid, ')
          ..write('datetime: $datetime, ')
          ..write('placeUuid: $placeUuid, ')
          ..write('isComplete: $isComplete')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, datetime, placeUuid, isComplete);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is OrderPlaceData &&
          other.uuid == this.uuid &&
          other.datetime == this.datetime &&
          other.placeUuid == this.placeUuid &&
          other.isComplete == this.isComplete);
}

class OrderPlaceCompanion extends UpdateCompanion<OrderPlaceData> {
  final Value<String> uuid;
  final Value<int> datetime;
  final Value<String> placeUuid;
  final Value<int> isComplete;
  final Value<int> rowid;
  const OrderPlaceCompanion({
    this.uuid = const Value.absent(),
    this.datetime = const Value.absent(),
    this.placeUuid = const Value.absent(),
    this.isComplete = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  OrderPlaceCompanion.insert({
    required String uuid,
    required int datetime,
    required String placeUuid,
    required int isComplete,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        datetime = Value(datetime),
        placeUuid = Value(placeUuid),
        isComplete = Value(isComplete);
  static Insertable<OrderPlaceData> custom({
    Expression<String>? uuid,
    Expression<int>? datetime,
    Expression<String>? placeUuid,
    Expression<int>? isComplete,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'Uuid': uuid,
      if (datetime != null) 'Datetime': datetime,
      if (placeUuid != null) 'PlaceUuid': placeUuid,
      if (isComplete != null) 'IsComplete': isComplete,
      if (rowid != null) 'rowid': rowid,
    });
  }

  OrderPlaceCompanion copyWith(
      {Value<String>? uuid,
      Value<int>? datetime,
      Value<String>? placeUuid,
      Value<int>? isComplete,
      Value<int>? rowid}) {
    return OrderPlaceCompanion(
      uuid: uuid ?? this.uuid,
      datetime: datetime ?? this.datetime,
      placeUuid: placeUuid ?? this.placeUuid,
      isComplete: isComplete ?? this.isComplete,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['Uuid'] = Variable<String>(uuid.value);
    }
    if (datetime.present) {
      map['Datetime'] = Variable<int>(datetime.value);
    }
    if (placeUuid.present) {
      map['PlaceUuid'] = Variable<String>(placeUuid.value);
    }
    if (isComplete.present) {
      map['IsComplete'] = Variable<int>(isComplete.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('OrderPlaceCompanion(')
          ..write('uuid: $uuid, ')
          ..write('datetime: $datetime, ')
          ..write('placeUuid: $placeUuid, ')
          ..write('isComplete: $isComplete, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class Product extends Table with TableInfo<Product, ProductData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Product(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'Uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL UNIQUE');
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'Name', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL UNIQUE');
  static const VerificationMeta _priceMeta = const VerificationMeta('price');
  late final GeneratedColumn<int> price = GeneratedColumn<int>(
      'Price', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL');
  static const VerificationMeta _priceCoinsMeta =
      const VerificationMeta('priceCoins');
  late final GeneratedColumn<int> priceCoins = GeneratedColumn<int>(
      'PriceCoins', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL');
  static const VerificationMeta _productTypeUuidMeta =
      const VerificationMeta('productTypeUuid');
  late final GeneratedColumn<String> productTypeUuid = GeneratedColumn<String>(
      'ProductTypeUuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL');
  @override
  List<GeneratedColumn> get $columns =>
      [uuid, name, price, priceCoins, productTypeUuid];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'Product';
  @override
  VerificationContext validateIntegrity(Insertable<ProductData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('Uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['Uuid']!, _uuidMeta));
    } else if (isInserting) {
      context.missing(_uuidMeta);
    }
    if (data.containsKey('Name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['Name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('Price')) {
      context.handle(
          _priceMeta, price.isAcceptableOrUnknown(data['Price']!, _priceMeta));
    } else if (isInserting) {
      context.missing(_priceMeta);
    }
    if (data.containsKey('PriceCoins')) {
      context.handle(
          _priceCoinsMeta,
          priceCoins.isAcceptableOrUnknown(
              data['PriceCoins']!, _priceCoinsMeta));
    } else if (isInserting) {
      context.missing(_priceCoinsMeta);
    }
    if (data.containsKey('ProductTypeUuid')) {
      context.handle(
          _productTypeUuidMeta,
          productTypeUuid.isAcceptableOrUnknown(
              data['ProductTypeUuid']!, _productTypeUuidMeta));
    } else if (isInserting) {
      context.missing(_productTypeUuidMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  ProductData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ProductData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Name'])!,
      price: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}Price'])!,
      priceCoins: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}PriceCoins'])!,
      productTypeUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}ProductTypeUuid'])!,
    );
  }

  @override
  Product createAlias(String alias) {
    return Product(attachedDatabase, alias);
  }

  @override
  List<String> get customConstraints => const ['PRIMARY KEY(Uuid)'];
  @override
  bool get dontWriteConstraints => true;
}

class ProductData extends DataClass implements Insertable<ProductData> {
  final String uuid;
  final String name;
  final int price;
  final int priceCoins;
  final String productTypeUuid;
  const ProductData(
      {required this.uuid,
      required this.name,
      required this.price,
      required this.priceCoins,
      required this.productTypeUuid});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['Uuid'] = Variable<String>(uuid);
    map['Name'] = Variable<String>(name);
    map['Price'] = Variable<int>(price);
    map['PriceCoins'] = Variable<int>(priceCoins);
    map['ProductTypeUuid'] = Variable<String>(productTypeUuid);
    return map;
  }

  ProductCompanion toCompanion(bool nullToAbsent) {
    return ProductCompanion(
      uuid: Value(uuid),
      name: Value(name),
      price: Value(price),
      priceCoins: Value(priceCoins),
      productTypeUuid: Value(productTypeUuid),
    );
  }

  factory ProductData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ProductData(
      uuid: serializer.fromJson<String>(json['Uuid']),
      name: serializer.fromJson<String>(json['Name']),
      price: serializer.fromJson<int>(json['Price']),
      priceCoins: serializer.fromJson<int>(json['PriceCoins']),
      productTypeUuid: serializer.fromJson<String>(json['ProductTypeUuid']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'Uuid': serializer.toJson<String>(uuid),
      'Name': serializer.toJson<String>(name),
      'Price': serializer.toJson<int>(price),
      'PriceCoins': serializer.toJson<int>(priceCoins),
      'ProductTypeUuid': serializer.toJson<String>(productTypeUuid),
    };
  }

  ProductData copyWith(
          {String? uuid,
          String? name,
          int? price,
          int? priceCoins,
          String? productTypeUuid}) =>
      ProductData(
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        price: price ?? this.price,
        priceCoins: priceCoins ?? this.priceCoins,
        productTypeUuid: productTypeUuid ?? this.productTypeUuid,
      );
  @override
  String toString() {
    return (StringBuffer('ProductData(')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('price: $price, ')
          ..write('priceCoins: $priceCoins, ')
          ..write('productTypeUuid: $productTypeUuid')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(uuid, name, price, priceCoins, productTypeUuid);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ProductData &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.price == this.price &&
          other.priceCoins == this.priceCoins &&
          other.productTypeUuid == this.productTypeUuid);
}

class ProductCompanion extends UpdateCompanion<ProductData> {
  final Value<String> uuid;
  final Value<String> name;
  final Value<int> price;
  final Value<int> priceCoins;
  final Value<String> productTypeUuid;
  final Value<int> rowid;
  const ProductCompanion({
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.price = const Value.absent(),
    this.priceCoins = const Value.absent(),
    this.productTypeUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  ProductCompanion.insert({
    required String uuid,
    required String name,
    required int price,
    required int priceCoins,
    required String productTypeUuid,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name),
        price = Value(price),
        priceCoins = Value(priceCoins),
        productTypeUuid = Value(productTypeUuid);
  static Insertable<ProductData> custom({
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<int>? price,
    Expression<int>? priceCoins,
    Expression<String>? productTypeUuid,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'Uuid': uuid,
      if (name != null) 'Name': name,
      if (price != null) 'Price': price,
      if (priceCoins != null) 'PriceCoins': priceCoins,
      if (productTypeUuid != null) 'ProductTypeUuid': productTypeUuid,
      if (rowid != null) 'rowid': rowid,
    });
  }

  ProductCompanion copyWith(
      {Value<String>? uuid,
      Value<String>? name,
      Value<int>? price,
      Value<int>? priceCoins,
      Value<String>? productTypeUuid,
      Value<int>? rowid}) {
    return ProductCompanion(
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      price: price ?? this.price,
      priceCoins: priceCoins ?? this.priceCoins,
      productTypeUuid: productTypeUuid ?? this.productTypeUuid,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['Uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['Name'] = Variable<String>(name.value);
    }
    if (price.present) {
      map['Price'] = Variable<int>(price.value);
    }
    if (priceCoins.present) {
      map['PriceCoins'] = Variable<int>(priceCoins.value);
    }
    if (productTypeUuid.present) {
      map['ProductTypeUuid'] = Variable<String>(productTypeUuid.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ProductCompanion(')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('price: $price, ')
          ..write('priceCoins: $priceCoins, ')
          ..write('productTypeUuid: $productTypeUuid, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class OrderProduct extends Table
    with TableInfo<OrderProduct, OrderProductData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  OrderProduct(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'Uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL UNIQUE');
  static const VerificationMeta _datetimeMeta =
      const VerificationMeta('datetime');
  late final GeneratedColumn<int> datetime = GeneratedColumn<int>(
      'Datetime', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL');
  static const VerificationMeta _productUuidMeta =
      const VerificationMeta('productUuid');
  late final GeneratedColumn<String> productUuid = GeneratedColumn<String>(
      'ProductUuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL');
  static const VerificationMeta _orderUuidMeta =
      const VerificationMeta('orderUuid');
  late final GeneratedColumn<String> orderUuid = GeneratedColumn<String>(
      'OrderUuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL');
  static const VerificationMeta _countMeta = const VerificationMeta('count');
  late final GeneratedColumn<int> count = GeneratedColumn<int>(
      'Count', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL CHECK (Count > 0)');
  @override
  List<GeneratedColumn> get $columns =>
      [uuid, datetime, productUuid, orderUuid, count];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'OrderProduct';
  @override
  VerificationContext validateIntegrity(Insertable<OrderProductData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('Uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['Uuid']!, _uuidMeta));
    } else if (isInserting) {
      context.missing(_uuidMeta);
    }
    if (data.containsKey('Datetime')) {
      context.handle(_datetimeMeta,
          datetime.isAcceptableOrUnknown(data['Datetime']!, _datetimeMeta));
    } else if (isInserting) {
      context.missing(_datetimeMeta);
    }
    if (data.containsKey('ProductUuid')) {
      context.handle(
          _productUuidMeta,
          productUuid.isAcceptableOrUnknown(
              data['ProductUuid']!, _productUuidMeta));
    } else if (isInserting) {
      context.missing(_productUuidMeta);
    }
    if (data.containsKey('OrderUuid')) {
      context.handle(_orderUuidMeta,
          orderUuid.isAcceptableOrUnknown(data['OrderUuid']!, _orderUuidMeta));
    } else if (isInserting) {
      context.missing(_orderUuidMeta);
    }
    if (data.containsKey('Count')) {
      context.handle(
          _countMeta, count.isAcceptableOrUnknown(data['Count']!, _countMeta));
    } else if (isInserting) {
      context.missing(_countMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  OrderProductData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return OrderProductData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Uuid'])!,
      datetime: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}Datetime'])!,
      productUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}ProductUuid'])!,
      orderUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}OrderUuid'])!,
      count: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}Count'])!,
    );
  }

  @override
  OrderProduct createAlias(String alias) {
    return OrderProduct(attachedDatabase, alias);
  }

  @override
  List<String> get customConstraints => const [
        'PRIMARY KEY(Uuid)',
        'FOREIGN KEY(OrderUuid)REFERENCES OrderPlace(Uuid)',
        'FOREIGN KEY(ProductUuid)REFERENCES Product(Uuid)'
      ];
  @override
  bool get dontWriteConstraints => true;
}

class OrderProductData extends DataClass
    implements Insertable<OrderProductData> {
  final String uuid;
  final int datetime;
  final String productUuid;
  final String orderUuid;
  final int count;
  const OrderProductData(
      {required this.uuid,
      required this.datetime,
      required this.productUuid,
      required this.orderUuid,
      required this.count});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['Uuid'] = Variable<String>(uuid);
    map['Datetime'] = Variable<int>(datetime);
    map['ProductUuid'] = Variable<String>(productUuid);
    map['OrderUuid'] = Variable<String>(orderUuid);
    map['Count'] = Variable<int>(count);
    return map;
  }

  OrderProductCompanion toCompanion(bool nullToAbsent) {
    return OrderProductCompanion(
      uuid: Value(uuid),
      datetime: Value(datetime),
      productUuid: Value(productUuid),
      orderUuid: Value(orderUuid),
      count: Value(count),
    );
  }

  factory OrderProductData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return OrderProductData(
      uuid: serializer.fromJson<String>(json['Uuid']),
      datetime: serializer.fromJson<int>(json['Datetime']),
      productUuid: serializer.fromJson<String>(json['ProductUuid']),
      orderUuid: serializer.fromJson<String>(json['OrderUuid']),
      count: serializer.fromJson<int>(json['Count']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'Uuid': serializer.toJson<String>(uuid),
      'Datetime': serializer.toJson<int>(datetime),
      'ProductUuid': serializer.toJson<String>(productUuid),
      'OrderUuid': serializer.toJson<String>(orderUuid),
      'Count': serializer.toJson<int>(count),
    };
  }

  OrderProductData copyWith(
          {String? uuid,
          int? datetime,
          String? productUuid,
          String? orderUuid,
          int? count}) =>
      OrderProductData(
        uuid: uuid ?? this.uuid,
        datetime: datetime ?? this.datetime,
        productUuid: productUuid ?? this.productUuid,
        orderUuid: orderUuid ?? this.orderUuid,
        count: count ?? this.count,
      );
  @override
  String toString() {
    return (StringBuffer('OrderProductData(')
          ..write('uuid: $uuid, ')
          ..write('datetime: $datetime, ')
          ..write('productUuid: $productUuid, ')
          ..write('orderUuid: $orderUuid, ')
          ..write('count: $count')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(uuid, datetime, productUuid, orderUuid, count);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is OrderProductData &&
          other.uuid == this.uuid &&
          other.datetime == this.datetime &&
          other.productUuid == this.productUuid &&
          other.orderUuid == this.orderUuid &&
          other.count == this.count);
}

class OrderProductCompanion extends UpdateCompanion<OrderProductData> {
  final Value<String> uuid;
  final Value<int> datetime;
  final Value<String> productUuid;
  final Value<String> orderUuid;
  final Value<int> count;
  final Value<int> rowid;
  const OrderProductCompanion({
    this.uuid = const Value.absent(),
    this.datetime = const Value.absent(),
    this.productUuid = const Value.absent(),
    this.orderUuid = const Value.absent(),
    this.count = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  OrderProductCompanion.insert({
    required String uuid,
    required int datetime,
    required String productUuid,
    required String orderUuid,
    required int count,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        datetime = Value(datetime),
        productUuid = Value(productUuid),
        orderUuid = Value(orderUuid),
        count = Value(count);
  static Insertable<OrderProductData> custom({
    Expression<String>? uuid,
    Expression<int>? datetime,
    Expression<String>? productUuid,
    Expression<String>? orderUuid,
    Expression<int>? count,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'Uuid': uuid,
      if (datetime != null) 'Datetime': datetime,
      if (productUuid != null) 'ProductUuid': productUuid,
      if (orderUuid != null) 'OrderUuid': orderUuid,
      if (count != null) 'Count': count,
      if (rowid != null) 'rowid': rowid,
    });
  }

  OrderProductCompanion copyWith(
      {Value<String>? uuid,
      Value<int>? datetime,
      Value<String>? productUuid,
      Value<String>? orderUuid,
      Value<int>? count,
      Value<int>? rowid}) {
    return OrderProductCompanion(
      uuid: uuid ?? this.uuid,
      datetime: datetime ?? this.datetime,
      productUuid: productUuid ?? this.productUuid,
      orderUuid: orderUuid ?? this.orderUuid,
      count: count ?? this.count,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['Uuid'] = Variable<String>(uuid.value);
    }
    if (datetime.present) {
      map['Datetime'] = Variable<int>(datetime.value);
    }
    if (productUuid.present) {
      map['ProductUuid'] = Variable<String>(productUuid.value);
    }
    if (orderUuid.present) {
      map['OrderUuid'] = Variable<String>(orderUuid.value);
    }
    if (count.present) {
      map['Count'] = Variable<int>(count.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('OrderProductCompanion(')
          ..write('uuid: $uuid, ')
          ..write('datetime: $datetime, ')
          ..write('productUuid: $productUuid, ')
          ..write('orderUuid: $orderUuid, ')
          ..write('count: $count, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class Place extends Table with TableInfo<Place, PlaceData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Place(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'Uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL UNIQUE');
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'Name', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL UNIQUE');
  @override
  List<GeneratedColumn> get $columns => [uuid, name];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'Place';
  @override
  VerificationContext validateIntegrity(Insertable<PlaceData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('Uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['Uuid']!, _uuidMeta));
    } else if (isInserting) {
      context.missing(_uuidMeta);
    }
    if (data.containsKey('Name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['Name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  PlaceData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return PlaceData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Name'])!,
    );
  }

  @override
  Place createAlias(String alias) {
    return Place(attachedDatabase, alias);
  }

  @override
  List<String> get customConstraints => const ['PRIMARY KEY(Uuid)'];
  @override
  bool get dontWriteConstraints => true;
}

class PlaceData extends DataClass implements Insertable<PlaceData> {
  final String uuid;
  final String name;
  const PlaceData({required this.uuid, required this.name});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['Uuid'] = Variable<String>(uuid);
    map['Name'] = Variable<String>(name);
    return map;
  }

  PlaceCompanion toCompanion(bool nullToAbsent) {
    return PlaceCompanion(
      uuid: Value(uuid),
      name: Value(name),
    );
  }

  factory PlaceData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return PlaceData(
      uuid: serializer.fromJson<String>(json['Uuid']),
      name: serializer.fromJson<String>(json['Name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'Uuid': serializer.toJson<String>(uuid),
      'Name': serializer.toJson<String>(name),
    };
  }

  PlaceData copyWith({String? uuid, String? name}) => PlaceData(
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('PlaceData(')
          ..write('uuid: $uuid, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, name);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is PlaceData &&
          other.uuid == this.uuid &&
          other.name == this.name);
}

class PlaceCompanion extends UpdateCompanion<PlaceData> {
  final Value<String> uuid;
  final Value<String> name;
  final Value<int> rowid;
  const PlaceCompanion({
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  PlaceCompanion.insert({
    required String uuid,
    required String name,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name);
  static Insertable<PlaceData> custom({
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'Uuid': uuid,
      if (name != null) 'Name': name,
      if (rowid != null) 'rowid': rowid,
    });
  }

  PlaceCompanion copyWith(
      {Value<String>? uuid, Value<String>? name, Value<int>? rowid}) {
    return PlaceCompanion(
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['Uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['Name'] = Variable<String>(name.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PlaceCompanion(')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class ProductType extends Table with TableInfo<ProductType, ProductTypeData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  ProductType(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'Uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL UNIQUE');
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'Name', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      $customConstraints: '');
  @override
  List<GeneratedColumn> get $columns => [uuid, name];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'ProductType';
  @override
  VerificationContext validateIntegrity(Insertable<ProductTypeData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('Uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['Uuid']!, _uuidMeta));
    } else if (isInserting) {
      context.missing(_uuidMeta);
    }
    if (data.containsKey('Name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['Name']!, _nameMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  ProductTypeData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ProductTypeData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Name']),
    );
  }

  @override
  ProductType createAlias(String alias) {
    return ProductType(attachedDatabase, alias);
  }

  @override
  List<String> get customConstraints => const ['PRIMARY KEY(Uuid)'];
  @override
  bool get dontWriteConstraints => true;
}

class ProductTypeData extends DataClass implements Insertable<ProductTypeData> {
  final String uuid;
  final String? name;
  const ProductTypeData({required this.uuid, this.name});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['Uuid'] = Variable<String>(uuid);
    if (!nullToAbsent || name != null) {
      map['Name'] = Variable<String>(name);
    }
    return map;
  }

  ProductTypeCompanion toCompanion(bool nullToAbsent) {
    return ProductTypeCompanion(
      uuid: Value(uuid),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
    );
  }

  factory ProductTypeData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ProductTypeData(
      uuid: serializer.fromJson<String>(json['Uuid']),
      name: serializer.fromJson<String?>(json['Name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'Uuid': serializer.toJson<String>(uuid),
      'Name': serializer.toJson<String?>(name),
    };
  }

  ProductTypeData copyWith(
          {String? uuid, Value<String?> name = const Value.absent()}) =>
      ProductTypeData(
        uuid: uuid ?? this.uuid,
        name: name.present ? name.value : this.name,
      );
  @override
  String toString() {
    return (StringBuffer('ProductTypeData(')
          ..write('uuid: $uuid, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, name);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ProductTypeData &&
          other.uuid == this.uuid &&
          other.name == this.name);
}

class ProductTypeCompanion extends UpdateCompanion<ProductTypeData> {
  final Value<String> uuid;
  final Value<String?> name;
  final Value<int> rowid;
  const ProductTypeCompanion({
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  ProductTypeCompanion.insert({
    required String uuid,
    this.name = const Value.absent(),
    this.rowid = const Value.absent(),
  }) : uuid = Value(uuid);
  static Insertable<ProductTypeData> custom({
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'Uuid': uuid,
      if (name != null) 'Name': name,
      if (rowid != null) 'rowid': rowid,
    });
  }

  ProductTypeCompanion copyWith(
      {Value<String>? uuid, Value<String?>? name, Value<int>? rowid}) {
    return ProductTypeCompanion(
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['Uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['Name'] = Variable<String>(name.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ProductTypeCompanion(')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class OrderPlaceViewData extends DataClass {
  final String uuid;
  final int datetime;
  final int isComplete;
  final String placeUuid;
  final String name;
  const OrderPlaceViewData(
      {required this.uuid,
      required this.datetime,
      required this.isComplete,
      required this.placeUuid,
      required this.name});
  factory OrderPlaceViewData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return OrderPlaceViewData(
      uuid: serializer.fromJson<String>(json['Uuid']),
      datetime: serializer.fromJson<int>(json['Datetime']),
      isComplete: serializer.fromJson<int>(json['IsComplete']),
      placeUuid: serializer.fromJson<String>(json['PlaceUuid']),
      name: serializer.fromJson<String>(json['Name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'Uuid': serializer.toJson<String>(uuid),
      'Datetime': serializer.toJson<int>(datetime),
      'IsComplete': serializer.toJson<int>(isComplete),
      'PlaceUuid': serializer.toJson<String>(placeUuid),
      'Name': serializer.toJson<String>(name),
    };
  }

  OrderPlaceViewData copyWith(
          {String? uuid,
          int? datetime,
          int? isComplete,
          String? placeUuid,
          String? name}) =>
      OrderPlaceViewData(
        uuid: uuid ?? this.uuid,
        datetime: datetime ?? this.datetime,
        isComplete: isComplete ?? this.isComplete,
        placeUuid: placeUuid ?? this.placeUuid,
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('OrderPlaceViewData(')
          ..write('uuid: $uuid, ')
          ..write('datetime: $datetime, ')
          ..write('isComplete: $isComplete, ')
          ..write('placeUuid: $placeUuid, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, datetime, isComplete, placeUuid, name);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is OrderPlaceViewData &&
          other.uuid == this.uuid &&
          other.datetime == this.datetime &&
          other.isComplete == this.isComplete &&
          other.placeUuid == this.placeUuid &&
          other.name == this.name);
}

class OrderPlaceView extends ViewInfo<OrderPlaceView, OrderPlaceViewData>
    implements HasResultSet {
  final String? _alias;
  @override
  final _$AppDatabase attachedDatabase;
  OrderPlaceView(this.attachedDatabase, [this._alias]);
  @override
  List<GeneratedColumn> get $columns =>
      [uuid, datetime, isComplete, placeUuid, name];
  @override
  String get aliasedName => _alias ?? entityName;
  @override
  String get entityName => 'OrderPlaceView';
  @override
  Map<SqlDialect, String> get createViewStatements => {
        SqlDialect.sqlite:
            'CREATE VIEW IF NOT EXISTS OrderPlaceView AS SELECT OrderPlace.Uuid, OrderPlace.Datetime, OrderPlace.IsComplete, OrderPlace.PlaceUuid, Place.Name FROM OrderPlace INNER JOIN Place ON OrderPlace.PlaceUuid = Place.Uuid',
      };
  @override
  OrderPlaceView get asDslTable => this;
  @override
  OrderPlaceViewData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return OrderPlaceViewData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Uuid'])!,
      datetime: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}Datetime'])!,
      isComplete: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}IsComplete'])!,
      placeUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}PlaceUuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Name'])!,
    );
  }

  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'Uuid', aliasedName, false,
      type: DriftSqlType.string);
  late final GeneratedColumn<int> datetime = GeneratedColumn<int>(
      'Datetime', aliasedName, false,
      type: DriftSqlType.int);
  late final GeneratedColumn<int> isComplete = GeneratedColumn<int>(
      'IsComplete', aliasedName, false,
      type: DriftSqlType.int);
  late final GeneratedColumn<String> placeUuid = GeneratedColumn<String>(
      'PlaceUuid', aliasedName, false,
      type: DriftSqlType.string);
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'Name', aliasedName, false,
      type: DriftSqlType.string);
  @override
  OrderPlaceView createAlias(String alias) {
    return OrderPlaceView(attachedDatabase, alias);
  }

  @override
  Query? get query => null;
  @override
  Set<String> get readTables => const {'OrderPlace', 'Place'};
}

class ProductViewData extends DataClass {
  final String uuid;
  final String name;
  final int price;
  final int priceCoins;
  final String productTypeUuid;
  final String? productTypeName;
  const ProductViewData(
      {required this.uuid,
      required this.name,
      required this.price,
      required this.priceCoins,
      required this.productTypeUuid,
      this.productTypeName});
  factory ProductViewData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ProductViewData(
      uuid: serializer.fromJson<String>(json['Uuid']),
      name: serializer.fromJson<String>(json['Name']),
      price: serializer.fromJson<int>(json['Price']),
      priceCoins: serializer.fromJson<int>(json['PriceCoins']),
      productTypeUuid: serializer.fromJson<String>(json['ProductTypeUuid']),
      productTypeName: serializer.fromJson<String?>(json['ProductTypeName']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'Uuid': serializer.toJson<String>(uuid),
      'Name': serializer.toJson<String>(name),
      'Price': serializer.toJson<int>(price),
      'PriceCoins': serializer.toJson<int>(priceCoins),
      'ProductTypeUuid': serializer.toJson<String>(productTypeUuid),
      'ProductTypeName': serializer.toJson<String?>(productTypeName),
    };
  }

  ProductViewData copyWith(
          {String? uuid,
          String? name,
          int? price,
          int? priceCoins,
          String? productTypeUuid,
          Value<String?> productTypeName = const Value.absent()}) =>
      ProductViewData(
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        price: price ?? this.price,
        priceCoins: priceCoins ?? this.priceCoins,
        productTypeUuid: productTypeUuid ?? this.productTypeUuid,
        productTypeName: productTypeName.present
            ? productTypeName.value
            : this.productTypeName,
      );
  @override
  String toString() {
    return (StringBuffer('ProductViewData(')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('price: $price, ')
          ..write('priceCoins: $priceCoins, ')
          ..write('productTypeUuid: $productTypeUuid, ')
          ..write('productTypeName: $productTypeName')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      uuid, name, price, priceCoins, productTypeUuid, productTypeName);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ProductViewData &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.price == this.price &&
          other.priceCoins == this.priceCoins &&
          other.productTypeUuid == this.productTypeUuid &&
          other.productTypeName == this.productTypeName);
}

class ProductView extends ViewInfo<ProductView, ProductViewData>
    implements HasResultSet {
  final String? _alias;
  @override
  final _$AppDatabase attachedDatabase;
  ProductView(this.attachedDatabase, [this._alias]);
  @override
  List<GeneratedColumn> get $columns =>
      [uuid, name, price, priceCoins, productTypeUuid, productTypeName];
  @override
  String get aliasedName => _alias ?? entityName;
  @override
  String get entityName => 'ProductView';
  @override
  Map<SqlDialect, String> get createViewStatements => {
        SqlDialect.sqlite:
            'CREATE VIEW IF NOT EXISTS ProductView AS SELECT Product.Uuid, Product.Name, Product.Price, Product.PriceCoins, Product.ProductTypeUuid, ProductType.Name AS ProductTypeName FROM Product INNER JOIN ProductType ON Product.ProductTypeUuid = ProductType.Uuid',
      };
  @override
  ProductView get asDslTable => this;
  @override
  ProductViewData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ProductViewData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Name'])!,
      price: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}Price'])!,
      priceCoins: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}PriceCoins'])!,
      productTypeUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}ProductTypeUuid'])!,
      productTypeName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}ProductTypeName']),
    );
  }

  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'Uuid', aliasedName, false,
      type: DriftSqlType.string);
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'Name', aliasedName, false,
      type: DriftSqlType.string);
  late final GeneratedColumn<int> price =
      GeneratedColumn<int>('Price', aliasedName, false, type: DriftSqlType.int);
  late final GeneratedColumn<int> priceCoins = GeneratedColumn<int>(
      'PriceCoins', aliasedName, false,
      type: DriftSqlType.int);
  late final GeneratedColumn<String> productTypeUuid = GeneratedColumn<String>(
      'ProductTypeUuid', aliasedName, false,
      type: DriftSqlType.string);
  late final GeneratedColumn<String> productTypeName = GeneratedColumn<String>(
      'ProductTypeName', aliasedName, true,
      type: DriftSqlType.string);
  @override
  ProductView createAlias(String alias) {
    return ProductView(attachedDatabase, alias);
  }

  @override
  Query? get query => null;
  @override
  Set<String> get readTables => const {'Product', 'ProductType'};
}

class OrderProductViewData extends DataClass {
  final String uuid;
  final int count;
  final int datetime;
  final String orderUuid;
  final String productUuid;
  final String name;
  final int price;
  final int priceCoins;
  final String productTypeUuid;
  final String? productTypeName;
  const OrderProductViewData(
      {required this.uuid,
      required this.count,
      required this.datetime,
      required this.orderUuid,
      required this.productUuid,
      required this.name,
      required this.price,
      required this.priceCoins,
      required this.productTypeUuid,
      this.productTypeName});
  factory OrderProductViewData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return OrderProductViewData(
      uuid: serializer.fromJson<String>(json['Uuid']),
      count: serializer.fromJson<int>(json['Count']),
      datetime: serializer.fromJson<int>(json['Datetime']),
      orderUuid: serializer.fromJson<String>(json['OrderUuid']),
      productUuid: serializer.fromJson<String>(json['ProductUuid']),
      name: serializer.fromJson<String>(json['Name']),
      price: serializer.fromJson<int>(json['Price']),
      priceCoins: serializer.fromJson<int>(json['PriceCoins']),
      productTypeUuid: serializer.fromJson<String>(json['ProductTypeUuid']),
      productTypeName: serializer.fromJson<String?>(json['ProductTypeName']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'Uuid': serializer.toJson<String>(uuid),
      'Count': serializer.toJson<int>(count),
      'Datetime': serializer.toJson<int>(datetime),
      'OrderUuid': serializer.toJson<String>(orderUuid),
      'ProductUuid': serializer.toJson<String>(productUuid),
      'Name': serializer.toJson<String>(name),
      'Price': serializer.toJson<int>(price),
      'PriceCoins': serializer.toJson<int>(priceCoins),
      'ProductTypeUuid': serializer.toJson<String>(productTypeUuid),
      'ProductTypeName': serializer.toJson<String?>(productTypeName),
    };
  }

  OrderProductViewData copyWith(
          {String? uuid,
          int? count,
          int? datetime,
          String? orderUuid,
          String? productUuid,
          String? name,
          int? price,
          int? priceCoins,
          String? productTypeUuid,
          Value<String?> productTypeName = const Value.absent()}) =>
      OrderProductViewData(
        uuid: uuid ?? this.uuid,
        count: count ?? this.count,
        datetime: datetime ?? this.datetime,
        orderUuid: orderUuid ?? this.orderUuid,
        productUuid: productUuid ?? this.productUuid,
        name: name ?? this.name,
        price: price ?? this.price,
        priceCoins: priceCoins ?? this.priceCoins,
        productTypeUuid: productTypeUuid ?? this.productTypeUuid,
        productTypeName: productTypeName.present
            ? productTypeName.value
            : this.productTypeName,
      );
  @override
  String toString() {
    return (StringBuffer('OrderProductViewData(')
          ..write('uuid: $uuid, ')
          ..write('count: $count, ')
          ..write('datetime: $datetime, ')
          ..write('orderUuid: $orderUuid, ')
          ..write('productUuid: $productUuid, ')
          ..write('name: $name, ')
          ..write('price: $price, ')
          ..write('priceCoins: $priceCoins, ')
          ..write('productTypeUuid: $productTypeUuid, ')
          ..write('productTypeName: $productTypeName')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, count, datetime, orderUuid, productUuid,
      name, price, priceCoins, productTypeUuid, productTypeName);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is OrderProductViewData &&
          other.uuid == this.uuid &&
          other.count == this.count &&
          other.datetime == this.datetime &&
          other.orderUuid == this.orderUuid &&
          other.productUuid == this.productUuid &&
          other.name == this.name &&
          other.price == this.price &&
          other.priceCoins == this.priceCoins &&
          other.productTypeUuid == this.productTypeUuid &&
          other.productTypeName == this.productTypeName);
}

class OrderProductView extends ViewInfo<OrderProductView, OrderProductViewData>
    implements HasResultSet {
  final String? _alias;
  @override
  final _$AppDatabase attachedDatabase;
  OrderProductView(this.attachedDatabase, [this._alias]);
  @override
  List<GeneratedColumn> get $columns => [
        uuid,
        count,
        datetime,
        orderUuid,
        productUuid,
        name,
        price,
        priceCoins,
        productTypeUuid,
        productTypeName
      ];
  @override
  String get aliasedName => _alias ?? entityName;
  @override
  String get entityName => 'OrderProductView';
  @override
  Map<SqlDialect, String> get createViewStatements => {
        SqlDialect.sqlite:
            'CREATE VIEW IF NOT EXISTS OrderProductView AS SELECT OrderProduct.Uuid, OrderProduct.Count, OrderProduct.Datetime, OrderProduct.OrderUuid, ProductView.Uuid AS ProductUuid, ProductView.Name, ProductView.Price, ProductView.PriceCoins, ProductView.ProductTypeUuid, ProductView.ProductTypeName FROM OrderProduct INNER JOIN ProductView ON OrderProduct.ProductUuid = ProductView.Uuid',
      };
  @override
  OrderProductView get asDslTable => this;
  @override
  OrderProductViewData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return OrderProductViewData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Uuid'])!,
      count: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}Count'])!,
      datetime: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}Datetime'])!,
      orderUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}OrderUuid'])!,
      productUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}ProductUuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}Name'])!,
      price: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}Price'])!,
      priceCoins: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}PriceCoins'])!,
      productTypeUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}ProductTypeUuid'])!,
      productTypeName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}ProductTypeName']),
    );
  }

  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'Uuid', aliasedName, false,
      type: DriftSqlType.string);
  late final GeneratedColumn<int> count =
      GeneratedColumn<int>('Count', aliasedName, false, type: DriftSqlType.int);
  late final GeneratedColumn<int> datetime = GeneratedColumn<int>(
      'Datetime', aliasedName, false,
      type: DriftSqlType.int);
  late final GeneratedColumn<String> orderUuid = GeneratedColumn<String>(
      'OrderUuid', aliasedName, false,
      type: DriftSqlType.string);
  late final GeneratedColumn<String> productUuid = GeneratedColumn<String>(
      'ProductUuid', aliasedName, false,
      type: DriftSqlType.string);
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'Name', aliasedName, false,
      type: DriftSqlType.string);
  late final GeneratedColumn<int> price =
      GeneratedColumn<int>('Price', aliasedName, false, type: DriftSqlType.int);
  late final GeneratedColumn<int> priceCoins = GeneratedColumn<int>(
      'PriceCoins', aliasedName, false,
      type: DriftSqlType.int);
  late final GeneratedColumn<String> productTypeUuid = GeneratedColumn<String>(
      'ProductTypeUuid', aliasedName, false,
      type: DriftSqlType.string);
  late final GeneratedColumn<String> productTypeName = GeneratedColumn<String>(
      'ProductTypeName', aliasedName, true,
      type: DriftSqlType.string);
  @override
  OrderProductView createAlias(String alias) {
    return OrderProductView(attachedDatabase, alias);
  }

  @override
  Query? get query => null;
  @override
  Set<String> get readTables =>
      const {'OrderProduct', 'Product', 'ProductType'};
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(e);
  _$AppDatabaseManager get managers => _$AppDatabaseManager(this);
  late final OrderPlace orderPlace = OrderPlace(this);
  late final Product product = Product(this);
  late final OrderProduct orderProduct = OrderProduct(this);
  late final Place place = Place(this);
  late final ProductType productType = ProductType(this);
  late final OrderPlaceView orderPlaceView = OrderPlaceView(this);
  late final ProductView productView = ProductView(this);
  late final OrderProductView orderProductView = OrderProductView(this);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        orderPlace,
        product,
        orderProduct,
        place,
        productType,
        orderPlaceView,
        productView,
        orderProductView
      ];
}

typedef $OrderPlaceInsertCompanionBuilder = OrderPlaceCompanion Function({
  required String uuid,
  required int datetime,
  required String placeUuid,
  required int isComplete,
  Value<int> rowid,
});
typedef $OrderPlaceUpdateCompanionBuilder = OrderPlaceCompanion Function({
  Value<String> uuid,
  Value<int> datetime,
  Value<String> placeUuid,
  Value<int> isComplete,
  Value<int> rowid,
});

class $OrderPlaceTableManager extends RootTableManager<
    _$AppDatabase,
    OrderPlace,
    OrderPlaceData,
    $OrderPlaceFilterComposer,
    $OrderPlaceOrderingComposer,
    $OrderPlaceProcessedTableManager,
    $OrderPlaceInsertCompanionBuilder,
    $OrderPlaceUpdateCompanionBuilder> {
  $OrderPlaceTableManager(_$AppDatabase db, OrderPlace table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $OrderPlaceFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $OrderPlaceOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) => $OrderPlaceProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<int> datetime = const Value.absent(),
            Value<String> placeUuid = const Value.absent(),
            Value<int> isComplete = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              OrderPlaceCompanion(
            uuid: uuid,
            datetime: datetime,
            placeUuid: placeUuid,
            isComplete: isComplete,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            required String uuid,
            required int datetime,
            required String placeUuid,
            required int isComplete,
            Value<int> rowid = const Value.absent(),
          }) =>
              OrderPlaceCompanion.insert(
            uuid: uuid,
            datetime: datetime,
            placeUuid: placeUuid,
            isComplete: isComplete,
            rowid: rowid,
          ),
        ));
}

class $OrderPlaceProcessedTableManager extends ProcessedTableManager<
    _$AppDatabase,
    OrderPlace,
    OrderPlaceData,
    $OrderPlaceFilterComposer,
    $OrderPlaceOrderingComposer,
    $OrderPlaceProcessedTableManager,
    $OrderPlaceInsertCompanionBuilder,
    $OrderPlaceUpdateCompanionBuilder> {
  $OrderPlaceProcessedTableManager(super.$state);
}

class $OrderPlaceFilterComposer
    extends FilterComposer<_$AppDatabase, OrderPlace> {
  $OrderPlaceFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get datetime => $state.composableBuilder(
      column: $state.table.datetime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get placeUuid => $state.composableBuilder(
      column: $state.table.placeUuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get isComplete => $state.composableBuilder(
      column: $state.table.isComplete,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $OrderPlaceOrderingComposer
    extends OrderingComposer<_$AppDatabase, OrderPlace> {
  $OrderPlaceOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get datetime => $state.composableBuilder(
      column: $state.table.datetime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get placeUuid => $state.composableBuilder(
      column: $state.table.placeUuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get isComplete => $state.composableBuilder(
      column: $state.table.isComplete,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $ProductInsertCompanionBuilder = ProductCompanion Function({
  required String uuid,
  required String name,
  required int price,
  required int priceCoins,
  required String productTypeUuid,
  Value<int> rowid,
});
typedef $ProductUpdateCompanionBuilder = ProductCompanion Function({
  Value<String> uuid,
  Value<String> name,
  Value<int> price,
  Value<int> priceCoins,
  Value<String> productTypeUuid,
  Value<int> rowid,
});

class $ProductTableManager extends RootTableManager<
    _$AppDatabase,
    Product,
    ProductData,
    $ProductFilterComposer,
    $ProductOrderingComposer,
    $ProductProcessedTableManager,
    $ProductInsertCompanionBuilder,
    $ProductUpdateCompanionBuilder> {
  $ProductTableManager(_$AppDatabase db, Product table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer: $ProductFilterComposer(ComposerState(db, table)),
          orderingComposer: $ProductOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) => $ProductProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String> name = const Value.absent(),
            Value<int> price = const Value.absent(),
            Value<int> priceCoins = const Value.absent(),
            Value<String> productTypeUuid = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              ProductCompanion(
            uuid: uuid,
            name: name,
            price: price,
            priceCoins: priceCoins,
            productTypeUuid: productTypeUuid,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            required String uuid,
            required String name,
            required int price,
            required int priceCoins,
            required String productTypeUuid,
            Value<int> rowid = const Value.absent(),
          }) =>
              ProductCompanion.insert(
            uuid: uuid,
            name: name,
            price: price,
            priceCoins: priceCoins,
            productTypeUuid: productTypeUuid,
            rowid: rowid,
          ),
        ));
}

class $ProductProcessedTableManager extends ProcessedTableManager<
    _$AppDatabase,
    Product,
    ProductData,
    $ProductFilterComposer,
    $ProductOrderingComposer,
    $ProductProcessedTableManager,
    $ProductInsertCompanionBuilder,
    $ProductUpdateCompanionBuilder> {
  $ProductProcessedTableManager(super.$state);
}

class $ProductFilterComposer extends FilterComposer<_$AppDatabase, Product> {
  $ProductFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get price => $state.composableBuilder(
      column: $state.table.price,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get priceCoins => $state.composableBuilder(
      column: $state.table.priceCoins,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get productTypeUuid => $state.composableBuilder(
      column: $state.table.productTypeUuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $ProductOrderingComposer
    extends OrderingComposer<_$AppDatabase, Product> {
  $ProductOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get price => $state.composableBuilder(
      column: $state.table.price,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get priceCoins => $state.composableBuilder(
      column: $state.table.priceCoins,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get productTypeUuid => $state.composableBuilder(
      column: $state.table.productTypeUuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $OrderProductInsertCompanionBuilder = OrderProductCompanion Function({
  required String uuid,
  required int datetime,
  required String productUuid,
  required String orderUuid,
  required int count,
  Value<int> rowid,
});
typedef $OrderProductUpdateCompanionBuilder = OrderProductCompanion Function({
  Value<String> uuid,
  Value<int> datetime,
  Value<String> productUuid,
  Value<String> orderUuid,
  Value<int> count,
  Value<int> rowid,
});

class $OrderProductTableManager extends RootTableManager<
    _$AppDatabase,
    OrderProduct,
    OrderProductData,
    $OrderProductFilterComposer,
    $OrderProductOrderingComposer,
    $OrderProductProcessedTableManager,
    $OrderProductInsertCompanionBuilder,
    $OrderProductUpdateCompanionBuilder> {
  $OrderProductTableManager(_$AppDatabase db, OrderProduct table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $OrderProductFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $OrderProductOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) => $OrderProductProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<int> datetime = const Value.absent(),
            Value<String> productUuid = const Value.absent(),
            Value<String> orderUuid = const Value.absent(),
            Value<int> count = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              OrderProductCompanion(
            uuid: uuid,
            datetime: datetime,
            productUuid: productUuid,
            orderUuid: orderUuid,
            count: count,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            required String uuid,
            required int datetime,
            required String productUuid,
            required String orderUuid,
            required int count,
            Value<int> rowid = const Value.absent(),
          }) =>
              OrderProductCompanion.insert(
            uuid: uuid,
            datetime: datetime,
            productUuid: productUuid,
            orderUuid: orderUuid,
            count: count,
            rowid: rowid,
          ),
        ));
}

class $OrderProductProcessedTableManager extends ProcessedTableManager<
    _$AppDatabase,
    OrderProduct,
    OrderProductData,
    $OrderProductFilterComposer,
    $OrderProductOrderingComposer,
    $OrderProductProcessedTableManager,
    $OrderProductInsertCompanionBuilder,
    $OrderProductUpdateCompanionBuilder> {
  $OrderProductProcessedTableManager(super.$state);
}

class $OrderProductFilterComposer
    extends FilterComposer<_$AppDatabase, OrderProduct> {
  $OrderProductFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get datetime => $state.composableBuilder(
      column: $state.table.datetime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get productUuid => $state.composableBuilder(
      column: $state.table.productUuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get orderUuid => $state.composableBuilder(
      column: $state.table.orderUuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get count => $state.composableBuilder(
      column: $state.table.count,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $OrderProductOrderingComposer
    extends OrderingComposer<_$AppDatabase, OrderProduct> {
  $OrderProductOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get datetime => $state.composableBuilder(
      column: $state.table.datetime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get productUuid => $state.composableBuilder(
      column: $state.table.productUuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get orderUuid => $state.composableBuilder(
      column: $state.table.orderUuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get count => $state.composableBuilder(
      column: $state.table.count,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $PlaceInsertCompanionBuilder = PlaceCompanion Function({
  required String uuid,
  required String name,
  Value<int> rowid,
});
typedef $PlaceUpdateCompanionBuilder = PlaceCompanion Function({
  Value<String> uuid,
  Value<String> name,
  Value<int> rowid,
});

class $PlaceTableManager extends RootTableManager<
    _$AppDatabase,
    Place,
    PlaceData,
    $PlaceFilterComposer,
    $PlaceOrderingComposer,
    $PlaceProcessedTableManager,
    $PlaceInsertCompanionBuilder,
    $PlaceUpdateCompanionBuilder> {
  $PlaceTableManager(_$AppDatabase db, Place table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer: $PlaceFilterComposer(ComposerState(db, table)),
          orderingComposer: $PlaceOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) => $PlaceProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String> name = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              PlaceCompanion(
            uuid: uuid,
            name: name,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            required String uuid,
            required String name,
            Value<int> rowid = const Value.absent(),
          }) =>
              PlaceCompanion.insert(
            uuid: uuid,
            name: name,
            rowid: rowid,
          ),
        ));
}

class $PlaceProcessedTableManager extends ProcessedTableManager<
    _$AppDatabase,
    Place,
    PlaceData,
    $PlaceFilterComposer,
    $PlaceOrderingComposer,
    $PlaceProcessedTableManager,
    $PlaceInsertCompanionBuilder,
    $PlaceUpdateCompanionBuilder> {
  $PlaceProcessedTableManager(super.$state);
}

class $PlaceFilterComposer extends FilterComposer<_$AppDatabase, Place> {
  $PlaceFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $PlaceOrderingComposer extends OrderingComposer<_$AppDatabase, Place> {
  $PlaceOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $ProductTypeInsertCompanionBuilder = ProductTypeCompanion Function({
  required String uuid,
  Value<String?> name,
  Value<int> rowid,
});
typedef $ProductTypeUpdateCompanionBuilder = ProductTypeCompanion Function({
  Value<String> uuid,
  Value<String?> name,
  Value<int> rowid,
});

class $ProductTypeTableManager extends RootTableManager<
    _$AppDatabase,
    ProductType,
    ProductTypeData,
    $ProductTypeFilterComposer,
    $ProductTypeOrderingComposer,
    $ProductTypeProcessedTableManager,
    $ProductTypeInsertCompanionBuilder,
    $ProductTypeUpdateCompanionBuilder> {
  $ProductTypeTableManager(_$AppDatabase db, ProductType table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $ProductTypeFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $ProductTypeOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) => $ProductTypeProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<String> uuid = const Value.absent(),
            Value<String?> name = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              ProductTypeCompanion(
            uuid: uuid,
            name: name,
            rowid: rowid,
          ),
          getInsertCompanionBuilder: ({
            required String uuid,
            Value<String?> name = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              ProductTypeCompanion.insert(
            uuid: uuid,
            name: name,
            rowid: rowid,
          ),
        ));
}

class $ProductTypeProcessedTableManager extends ProcessedTableManager<
    _$AppDatabase,
    ProductType,
    ProductTypeData,
    $ProductTypeFilterComposer,
    $ProductTypeOrderingComposer,
    $ProductTypeProcessedTableManager,
    $ProductTypeInsertCompanionBuilder,
    $ProductTypeUpdateCompanionBuilder> {
  $ProductTypeProcessedTableManager(super.$state);
}

class $ProductTypeFilterComposer
    extends FilterComposer<_$AppDatabase, ProductType> {
  $ProductTypeFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $ProductTypeOrderingComposer
    extends OrderingComposer<_$AppDatabase, ProductType> {
  $ProductTypeOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

class _$AppDatabaseManager {
  final _$AppDatabase _db;
  _$AppDatabaseManager(this._db);
  $OrderPlaceTableManager get orderPlace =>
      $OrderPlaceTableManager(_db, _db.orderPlace);
  $ProductTableManager get product => $ProductTableManager(_db, _db.product);
  $OrderProductTableManager get orderProduct =>
      $OrderProductTableManager(_db, _db.orderProduct);
  $PlaceTableManager get place => $PlaceTableManager(_db, _db.place);
  $ProductTypeTableManager get productType =>
      $ProductTypeTableManager(_db, _db.productType);
}
