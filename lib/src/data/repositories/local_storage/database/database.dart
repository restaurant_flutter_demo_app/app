import 'package:drift/drift.dart';

import 'connection/connection.dart' as impl;

part 'database.g.dart';

@DriftDatabase(include: {'sql.drift'})
class AppDatabase extends _$AppDatabase {
  AppDatabase() : super(impl.connect()) {
    onForeignKeys();
  }

  @override
  int get schemaVersion => 1;

  void onForeignKeys() async {
    //await customStatement('PRAGMA foreign_keys = ON');
  }
}
