CREATE TABLE IF NOT EXISTS "OrderPlace" (
	"Uuid"	TEXT NOT NULL UNIQUE,
	"Datetime"	INTEGER NOT NULL,
	"PlaceUuid"	TEXT NOT NULL,
	"IsComplete"	INTEGER NOT NULL,
	PRIMARY KEY("Uuid")
);
CREATE TABLE IF NOT EXISTS "OrderProduct" (
	"Uuid"	TEXT NOT NULL UNIQUE,
	"Datetime"	INTEGER NOT NULL,
	"ProductUuid"	TEXT NOT NULL,
	"OrderUuid"	TEXT NOT NULL,
	"Count"	INTEGER NOT NULL CHECK(Count>0),
	PRIMARY KEY("Uuid"),
	FOREIGN KEY("OrderUuid") REFERENCES "OrderPlace"("Uuid"),
	FOREIGN KEY("ProductUuid") REFERENCES "Product"("Uuid")
);
CREATE TABLE IF NOT EXISTS "Place" (
	"Uuid"	TEXT NOT NULL UNIQUE,
	"Name"	TEXT NOT NULL UNIQUE,
	PRIMARY KEY("Uuid")
);
CREATE TABLE IF NOT EXISTS "Product" (
	"Uuid"	TEXT NOT NULL UNIQUE,
	"Name"	TEXT NOT NULL UNIQUE,
	"Price"	INTEGER NOT NULL,
	"PriceCoins"	INTEGER NOT NULL,
	"ProductTypeUuid"	TEXT NOT NULL,
	PRIMARY KEY("Uuid")
);
CREATE TABLE IF NOT EXISTS "ProductType" (
	"Uuid"	TEXT NOT NULL UNIQUE,
	"Name"	TEXT,
	PRIMARY KEY("Uuid")
);
CREATE VIEW IF NOT EXISTS OrderPlaceView
AS
SELECT
OrderPlace.Uuid,
OrderPlace.Datetime,
OrderPlace.IsComplete,
OrderPlace.PlaceUuid,
Place.Name
FROM OrderPlace
INNER join Place on OrderPlace.PlaceUuid= Place.Uuid;
CREATE VIEW IF NOT EXISTS OrderProductView
AS
SELECT
OrderProduct.Uuid,
OrderProduct.Count,
OrderProduct.Datetime,
OrderProduct.OrderUuid,
ProductView.Uuid as ProductUuid,
ProductView.Name,
ProductView.Price,
ProductView.PriceCoins,
ProductView.ProductTypeUuid,
ProductView.ProductTypeName
FROM OrderProduct
INNER join ProductView on OrderProduct.ProductUuid = ProductView.Uuid;
CREATE VIEW IF NOT EXISTS ProductView
AS
SELECT
Product.Uuid,
Product.Name,
Product.Price,
Product.PriceCoins,
Product.ProductTypeUuid,
ProductType.Name as ProductTypeName
FROM Product
INNER join ProductType on Product.ProductTypeUuid = ProductType.Uuid;
