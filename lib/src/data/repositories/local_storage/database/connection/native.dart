import 'dart:io';

import 'package:app/generated/assets.dart';
import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:sqlite3/sqlite3.dart';
import 'package:sqlite3_flutter_libs/sqlite3_flutter_libs.dart';

Future<File> get databaseFile async {
  // We use `path_provider` to find a suitable path to store our data in.
  final appDir = await getApplicationDocumentsDirectory();
  final dbPath = p.join(appDir.path, 'app.db');
  return File(dbPath);
}

/// Obtains a database connection for running drift in a Dart VM.
DatabaseConnection connect() {
  return DatabaseConnection.delayed(
    Future(
      () async {
        final file = await databaseFile;

        if (!await file.exists()) {
          /// Extract the pre-populated database file from assets
          final blob = await rootBundle.load(Assets.dbApp);
          final buffer = blob.buffer;

          await file.writeAsBytes(
            buffer.asUint8List(
              blob.offsetInBytes,
              blob.lengthInBytes,
            ),
          );
        } else if (Platform.isAndroid) {
          await applyWorkaroundToOpenSqlite3OnOldAndroidVersions();

          final cachebase = (await getTemporaryDirectory()).path;

          // We can't access /tmp on Android, which sqlite3 would try by default.
          // Explicitly tell it about the correct temporary directory.
          sqlite3.tempDirectory = cachebase;
        }

        return NativeDatabase.createBackgroundConnection(
          file,
        );
      },
    ),
  );
}
